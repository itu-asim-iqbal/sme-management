/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database_finalproject;

import java.awt.Label;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

/**
 *
 * @author MohammadAsim
 */
public class Awab_TimerThreadClass extends Thread
{   
    protected boolean isRunning;
    protected JLabel dateLabel;
    protected JLabel timeLabel;
    protected SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy");
    protected SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss a");
    
    protected static String dateString;
    protected static String timeString;
    

    public Awab_TimerThreadClass (JLabel dateLabel, JLabel timeLabel)
    {
            this.dateLabel = dateLabel;
            this.timeLabel = timeLabel;
            this.isRunning = true;
     }            

    Awab_TimerThreadClass(Label dateLabel, Label timeLabel) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
            
    @Override
    public void run() 
    {
            while (isRunning)
            {
                SwingUtilities.invokeLater(new Runnable() 
                {
                    @Override
                    public void run() 
                    {
                        Calendar currentCalendar = Calendar.getInstance();
                        Date currentTime = currentCalendar.getTime();
                        dateString = (String)(dateFormat.format(currentTime));
                        timeString = (String)(timeFormat.format(currentTime));
                        dateLabel.setText(dateString);
                        timeLabel.setText(timeString);
                    }
                });

                try 
                {
                    Thread.sleep(1000L);
                } 
                catch (InterruptedException e) 
                {
                    
                }
            }
        }

        public void setRunning(boolean isRunning) 
        {
            this.isRunning = isRunning;
        }

    }