package database_finalproject;
import java.sql.*;  
import static java.time.Clock.system;
import java.util.Vector;
import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;



class test
{
    public static Connection con;
    static Statement stmt;
    protected static String Schema="scott";
    protected static String USER = Asim_Database_FinalProject.qtm.USER ;
    protected static String PASS = Asim_Database_FinalProject.qtm.PASS ;
    protected static final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    protected static final String DB_URL = "jdbc:oracle:thin:@localhost:1521:orcl";
    protected static String Exception="";
    
    
//    public test ( )
//    {
//        con = Asim_Database_FinalProject.qtm.db ;
//    }
    
    public static void setHostURL (  )
    {
        closeDB( ) ;        
        initDB (  ) ;
    }
    
    public static void initDB (  )
    {
        Exception="";
        try 
        {
            Class.forName ( JDBC_DRIVER ) ;
        }
        catch (final Exception e) 
        {
            Exception=e.getMessage();
            System.out.println("Exception: "+e.getMessage());
            System.out.println("2222222");
            throw new RuntimeException("Driver failure");
        }
        
        try
        {
            con = DriverManager.getConnection ( DB_URL , USER , PASS ) ;
        }
        catch (final Exception e)
        {
            Exception=e.getMessage();
            System.out.println("Exception: "+e.getMessage());
            System.out.println("3333333");
            throw new RuntimeException("Login failure");
        }
        
        try
        {
            stmt = con.createStatement ( ) ;
        }
        catch (final Exception e) 
        {
            Exception=e.getMessage();
            System.out.println("Exception: "+e.getMessage());
            System.out.println("44444444");
            throw new RuntimeException("SQL failure");
        }
        try
        {
            alterSession();
        }
        catch (final Exception e) 
        {
            Exception=e.getMessage();
            System.out.println("Exception: "+e.getMessage());
            System.out.println("55555555");
            throw new RuntimeException("SQL failure");
        }        
    }
    
    public static void alterSession()
    {
        Exception="";
        try
        {
            stmt.execute( "alter session set current_schema="+Schema ) ;
            
        }
        catch ( Exception e )
        {
            Exception=e.getMessage();
            e.printStackTrace( ) ;
            throw new RuntimeException("SQL Exception");
        }
    }
    
    public static void closeDB ( ) 
    {
        Exception="";
        try 
        {
            if ( stmt != null )
            {
                stmt.close( ) ;
            }
            if ( con != null )
            {
                con.close( ) ;
                //USER = "" ;
                //PASS = "" ;
            }
        }
        catch (Exception e) 
        {
            Exception=e.getMessage();
            System.out.println("Could not close the current connection.");
            System.out.println("66666666");
            e.printStackTrace();
        }
    }
    
    public static void EditTable(String tableName , JTable uTable , DefaultTableModel tableModel, String inv_id, String col, String newv)
    {
        setHostURL (  ) ;
        try
        {
//            stmt = con.createStatement();
            String sql;
            sql = "Update Inventory set ";
            sql += col;
            sql+= " = ";
            sql+= newv;
            sql+= " where inv_id = ";
            sql+= inv_id;
            System.out.println(sql);
            ResultSet orderSet = stmt.executeQuery(sql);
            stmt.close();
                        
        }
         catch(SQLException se)
        {
            //Handle errors for JDBC
            se.printStackTrace();
        }
        catch(Exception e)
        {
            //Handle errors for Class.forName
            e.printStackTrace();
        }
         finally
        {
//finally block used to close resources
            try
            {
                if(stmt!=null)
                stmt.close();
            }
            catch(SQLException se2)
            {
                
            }// nothing we can do
            
        }
    }
    
    public static void Add(String tableName , JTable uTable , DefaultTableModel tableModel, String style, String type, String cost, String quantity, String grade)
    {
//        Statement stmt = null ;
        setHostURL ( ) ;
        try
        {
//            stmt = con.createStatement();
            String sql;
            sql = "Insert into Inventory (Type_inv , Quantity_inv , Cost_inv , Style_inv , Grade_inv) Values ('";
            sql += type;
            sql+= "'";
            sql+= ", ";
            sql += quantity;
            sql+= ", ";
            sql+= cost;
            sql+= ", '";
            sql+= style;
            sql+= "', '";
            sql+= grade;
            sql+= "')";
            System.out.println(sql);
            ResultSet orderSet = stmt.executeQuery(sql);
            stmt.close();
                        
        }
         catch(SQLException se)
        {
            //Handle errors for JDBC
            se.printStackTrace();
        }
        catch(Exception e)
        {
            //Handle errors for Class.forName
            e.printStackTrace();
        }
         finally
        {
//finally block used to close resources
            try
            {
                if(stmt!=null)
                stmt.close();
            }
            catch(SQLException se2)
            {
                
            }// nothing we can do
            
        }
    }
    
    
    public static void DeleteTable(String tableName , JTable uTable , DefaultTableModel tableModel, String inv_id)
    {
        setHostURL ( ) ;
//        Statement stmt = null ;
        try
        {
//            stmt = con.createStatement();
            String sql;
            sql = "delete from ";
            sql += tableName;
            sql += " where inv_id = ";
            sql += inv_id;
            ResultSet orderSet = stmt.executeQuery(sql);
            stmt.close();
                        
        }
         catch(SQLException se)
        {
            //Handle errors for JDBC
            JOptionPane.showMessageDialog(null, "ERROR: Unable to delete inventory. It is being used by a product");//, "ERROR: " , JOptionPane.INFORMATION_MESSAGE
            se.printStackTrace();
            
        }
        catch(Exception e)
        {
            //Handle errors for Class.forName
            e.printStackTrace();
        }
         finally
        {
//finally block used to close resources
            try
            {
                if(stmt!=null)
                stmt.close();
            }
            catch(SQLException se2)
            {
                
            }// nothing we can do
            
        }
    }
    
    public static void SearchTable(String tableName , JTable uTable , DefaultTableModel tableModel, String type , String inv_id)
    {
        setHostURL ( ) ;
        if(type.equals("Cost_Inv") || type.equals("Quantity_Inv"))
        {
//            Statement stmt = null ;
            try
            {
//                stmt = con.createStatement();
                String sql;
                sql = "SELECT * FROM ";
                sql += tableName;
                sql += " where ";
                sql+= type;
                String k[] = inv_id.split(" ");
                if(k.length == 2)
                {
                    sql+= " between ";
                    sql += k[0];
                    sql += " and ";
                    sql += k[1];
                }
                else
                {
                    sql+= " = " + inv_id;
                }
                ResultSet orderSet = stmt.executeQuery(sql);
                ResultSetMetaData metaData = orderSet.getMetaData();
                Vector<String> columnNames = new Vector<String>();
                int columnCount = metaData.getColumnCount();

                for (int i = 1; i <= columnCount; i++) 
                {
                    columnNames.add(metaData.getColumnName(i));        
                }
                Vector<Vector<Object>> data = new Vector<Vector<Object>>();
                while (orderSet.next()) 
                {
                    Vector<Object> vector = new Vector<Object>();
                    for (int i = 1; i <= columnCount; i++)
                    {
                        vector.add(orderSet.getObject(i));

                    }
                    data.add(vector);
                }
                tableModel.setDataVector(data,columnNames);      
                orderSet.close();
                stmt.close();

            }
             catch(SQLException se)
            {
                //Handle errors for JDBC
                se.printStackTrace();
            }
            catch(Exception e)
            {
                //Handle errors for Class.forName
                e.printStackTrace();
            }
             finally
            {
    //finally block used to close resources
                try
                {
                    if(stmt!=null)
                    stmt.close();
                }
                catch(SQLException se2)
                {

                }// nothing we can do

            }
        }
        
        if(type.equals("Type_Inv")  || type.equals("Grade_Inv")  || type.equals("Inv_Id"))
        { 
            setHostURL ( ) ;
//            Statement stmt = null ;
            try
            {
//                stmt = con.createStatement();
                String sql;
                sql = "SELECT * FROM ";
                sql += tableName;
                sql += " where upper(";
                sql+= type;
                sql+= ") = ";
                sql += "upper('";
                sql += inv_id;
                sql += "')";
                ResultSet orderSet = stmt.executeQuery(sql);
                ResultSetMetaData metaData = orderSet.getMetaData();
                Vector<String> columnNames = new Vector<String>();
                int columnCount = metaData.getColumnCount();

                for (int i = 1; i <= columnCount; i++) 
                {
                    columnNames.add(metaData.getColumnName(i));        
                }
                Vector<Vector<Object>> data = new Vector<Vector<Object>>();
                while (orderSet.next()) 
                {
                    Vector<Object> vector = new Vector<Object>();
                    for (int i = 1; i <= columnCount; i++)
                    {
                        vector.add(orderSet.getObject(i));

                    }
                    data.add(vector);
                }
                tableModel.setDataVector(data,columnNames);      
                orderSet.close();
                stmt.close();

            }
             catch(SQLException se)
            {
                //Handle errors for JDBC
                se.printStackTrace();
            }
            catch(Exception e)
            {
                //Handle errors for Class.forName
                e.printStackTrace();
            }
             finally
            {
    //finally block used to close resources
                try
                {
                    if(stmt!=null)
                    stmt.close();
                }
                catch(SQLException se2)
                {

                }// nothing we can do

            }
        }   
    }
    public static void setTable(String tableName , JTable uTable , DefaultTableModel tableModel)
    {
         setHostURL ( ) ;
         System.out.println ( USER + "          " + PASS) ;
       
//        Statement stmt = null ;
        try
        {
//            stmt = con.createStatement();
            String sql;
            sql = "SELECT * FROM " ;
            sql += tableName;
            ResultSet orderSet = stmt.executeQuery(sql);
            ResultSetMetaData metaData = orderSet.getMetaData();
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            
            for (int i = 1; i <= columnCount; i++) 
            {
                columnNames.add(metaData.getColumnName(i));        
            }
            Vector<Vector<Object>> data = new Vector<Vector<Object>>();
            while (orderSet.next()) 
            {
                Vector<Object> vector = new Vector<Object>();
                for (int i = 1; i <= columnCount; i++)
                {
                    vector.add(orderSet.getObject(i));
                    
                }
                data.add(vector);
            }
            tableModel.setDataVector(data,columnNames);      
            orderSet.close();
            stmt.close();
                        
        }
         catch(SQLException se)
        {
            //Handle errors for JDBC
            se.printStackTrace();
        }
        catch(Exception e)
        {
            //Handle errors for Class.forName
            e.printStackTrace();
        }
         finally
        {
//finally block used to close resources
            try
            {
                if(stmt!=null)
                stmt.close();
            }
            catch(SQLException se2)
            {
                
            }// nothing we can do
        }
    }

    /*public static void main(String args[]){  
//        try
//        { 
//            /*Shehryar_Supplies s = new Shehryar_Supplies();
//            s.setVisible(true);
            //Shehryar_Inventory I = new Shehryar_Inventory();
            //I.setVisible(true);
//            
            users_login u = new users_login();
            u.setVisible(true);
            
//            //step1 load the driver class  
//            Class.forName("oracle.jdbc.driver.OracleDriver");  
//
//            //step2 create  the connection object  
//            con=DriverManager.getConnection(  
//            "jdbc:oracle:thin:@localhost:1521:orcl","system","Dsa54321");  
//
//            //step3 create the statement object  
//            Statement stmt=con.createStatement();   
//
//            ResultSet rs=stmt.executeQuery("select * from Inventory");  
//            while(rs.next())  
//            System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));  
//            
//            //step5 close the connection object
//
//        }
//        catch(Exception e)
//        { 
//            System.out.println(e);
//        }  

    }  */
}
