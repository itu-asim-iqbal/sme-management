/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package database_finalproject;
import java.sql.*;  
class SubQueries{
        protected String Schema="scott";
    protected String Exception="";
    public SubQueries()
{

  
}  
 
String[] buttons(String[] arg)
{
    try{  
//step1 load the driver class  
Class.forName("oracle.jdbc.driver.OracleDriver");  
  
//step2 create  the connection object  
Connection con=DriverManager.getConnection(  
"jdbc:oracle:thin:@localhost:1521:orcl",Asim_Database_FinalProject.qtm.USER,Asim_Database_FinalProject.qtm.PASS);  
//step3 create the statement object  
Statement stmt=con.createStatement();  
int cx=0;  
alterSession(stmt);
//step4 execute query  
ResultSet rs=stmt.executeQuery("select Style_inv||'-'|| Grade_inv ||'-'|| Inv_id from INVENTORY where Type_inv = 'Buttons'");  
while(rs.next())
{
System.out.println(rs.getString(1));   // get this into array and send back array to populate combo box
arg[cx]=rs.getString(1);
cx++;

}
//step5 close the connection object  
con.close();  
return arg;
  
}catch(Exception t){ System.out.println(t);}  
  return arg;
}  

String[] thread(String[] arg)
{
    try{  
//step1 load the driver class  
Class.forName("oracle.jdbc.driver.OracleDriver");  
  
//step2 create  the connection object  
Connection con=DriverManager.getConnection(  
"jdbc:oracle:thin:@localhost:1521:orcl",Asim_Database_FinalProject.qtm.USER,Asim_Database_FinalProject.qtm.PASS);  

//step3 create the statement object  
Statement stmt=con.createStatement();  
int cx=0;  
alterSession(stmt);
//step4 execute query  
ResultSet rs=stmt.executeQuery("select Style_inv||'-'|| Grade_inv ||'-'|| Inv_id from INVENTORY where Type_inv = 'Thread'");  
while(rs.next())
{
System.out.println(rs.getString(1));   // get this into array and send back array to populate combo box
arg[cx]=rs.getString(1);
cx++;

}
  
//step5 close the connection object  
con.close();  
return arg;
  
}catch(Exception t){ System.out.println(t);}  
  return arg;
}  


String[] zipper(String[] arg)
{
    try{  
//step1 load the driver class  
Class.forName("oracle.jdbc.driver.OracleDriver");  
  
//step2 create  the connection object  
Connection con=DriverManager.getConnection(  
"jdbc:oracle:thin:@localhost:1521:orcl",Asim_Database_FinalProject.qtm.USER,Asim_Database_FinalProject.qtm.PASS);  
//step3 create the statement object  
Statement stmt=con.createStatement();  
alterSession(stmt);
int cx=0;  
//step4 execute query  
ResultSet rs=stmt.executeQuery("select Style_inv||'-'|| Grade_inv ||'-'|| Inv_id from INVENTORY where Type_inv = 'Zipper'");  
while(rs.next())
{
System.out.println(rs.getString(1));   // get this into array and send back array to populate combo box
arg[cx]=rs.getString(1);
cx++;

}
  
//step5 close the connection object  
con.close();  
return arg;
  
}catch(Exception t){ System.out.println(t);}  
  return arg;
}  



int buttonsnm()
{
    try{  
//step1 load the driver class  
Class.forName("oracle.jdbc.driver.OracleDriver");  
  
//step2 create  the connection object  
Connection con=DriverManager.getConnection(  
"jdbc:oracle:thin:@localhost:1521:orcl",Asim_Database_FinalProject.qtm.USER,Asim_Database_FinalProject.qtm.PASS);  


  
//step3 create the statement object  
Statement stmt=con.createStatement();  
alterSession(stmt);
int cx=0;  
//step4 execute query  
ResultSet rs=stmt.executeQuery("select * from INVENTORY where Type_inv = 'Buttons'");  
while(rs.next())
{
System.out.println(rs.getInt(1));   // get this into array and send back array to populate combo box
cx++;
}

System.out.println(cx);
   
//step5 close the connection object  
con.close();  
  return cx;
}catch(Exception t){ System.out.println(t);}  
  return 0;
}  

int zippersnm()
{
    try{  
//step1 load the driver class  
Class.forName("oracle.jdbc.driver.OracleDriver");  
  
//step2 create  the connection object  
Connection con=DriverManager.getConnection(  
"jdbc:oracle:thin:@localhost:1521:orcl",Asim_Database_FinalProject.qtm.USER,Asim_Database_FinalProject.qtm.PASS);  
  
//step3 create the statement object  
Statement stmt=con.createStatement();  
  alterSession(stmt);
//step4 execute query 
int cx=0;
ResultSet rs=stmt.executeQuery("select * from INVENTORY where Type_inv = 'Zipper'");  
while(rs.next())  
{
System.out.println(rs.getInt(1));   // get this into array and send back array to populate combo box
cx++;
}


   
//step5 close the connection object  
con.close();  
return cx;
  
}catch(Exception t){ System.out.println(t);}  
  return 0;
}  

int threadsnm()
{
    try{  
//step1 load the driver class  
Class.forName("oracle.jdbc.driver.OracleDriver");  
  
//step2 create  the connection object  
Connection con=DriverManager.getConnection(  
"jdbc:oracle:thin:@localhost:1521:orcl",Asim_Database_FinalProject.qtm.USER,Asim_Database_FinalProject.qtm.PASS);  
  
//step3 create the statement object  
Statement stmt=con.createStatement();  
alterSession(stmt);
  int cx=0;
//step4 execute query  
ResultSet rs=stmt.executeQuery("select * from INVENTORY where Type_inv = 'Thread'");  
while(rs.next())  
{
System.out.println(rs.getInt(1));   // get this into array and send back array to populate combo box
cx++;
}
  

   
//step5 close the connection object  
con.close();  
return cx;
  
}catch(Exception t){ System.out.println(t);}  
  return 0;
}  

int price(int inv)
{
    try{  
//step1 load the driver class  
Class.forName("oracle.jdbc.driver.OracleDriver");  
  
//step2 create  the connection object  
Connection con=DriverManager.getConnection(  
"jdbc:oracle:thin:@localhost:1521:orcl",Asim_Database_FinalProject.qtm.USER,Asim_Database_FinalProject.qtm.PASS);  
  
//step3 create the statement object  
Statement stmt=con.createStatement();  
  alterSession(stmt);
//step4 execute query  
ResultSet rs=stmt.executeQuery("select Cost_inv from INVENTORY where Inv_id = '"+inv+"'");  
while(rs.next())  
{
System.out.println(rs.getInt(1));   // get this into array and send back array to populate combo box
inv=Integer.parseInt( rs.getString(1));
}
  

   
//step5 close the connection object  
con.close();  
return inv;
  
}catch(Exception t){ System.out.println(t);}  
  return 0;
}  

public void alterSession(Statement st)
    {
        Exception="";
        try
        {
            st.execute( "alter session set current_schema="+Schema ) ;
            
        }
        catch ( Exception e )
        {
            Exception=e.getMessage();
            e.printStackTrace( ) ;
            throw new RuntimeException("SQL Exception");
        }
    }
    

}



