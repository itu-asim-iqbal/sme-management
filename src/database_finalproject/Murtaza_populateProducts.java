package database_finalproject;
import java.sql.*;
import java.util.Vector;
import javax.swing.JComboBox;

public class Murtaza_populateProducts {
    public Vector values;
    public Murtaza_populateProducts(JComboBox cb, String prod_id, Murtaza_edit_order_screen eos)
    {
        values=new Vector();
        dealProducts(cb, prod_id, eos);
    }
    private void dealProducts(JComboBox cb, String prod_id, Murtaza_edit_order_screen eos)
    {
        try
        {
//            Class.forName("oracle.jdbc.driver.OracleDriver");
//            Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl",Asim_Database_FinalProject.qtm.USER,Asim_Database_FinalProject.qtm.PASS);
             Asim_Database_FinalProject.qtm.initDB( Asim_Database_FinalProject.qtm.DB_URL ) ;
            Connection con = Asim_Database_FinalProject.qtm.db ;
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery("SELECT PROD_ID, PROD_NAME, PROD_COST FROM AK_PRODUCTS");  
            int x=0;
            while(rs.next())
            {
                String data[]=new String[rs.getMetaData().getColumnCount()];
                for(int i=1; i<=rs.getMetaData().getColumnCount(); i++)
                {
                    data[i-1]=rs.getString(i);
                }
                values.add(data);
                cb.addItem(data[0]+" - "+data[1]+" - "+data[2]);
                if(data[0].equals(prod_id))
                {
                    cb.setSelectedIndex(x);
                }
                x++;
            }
            //cb.setSelectedIndex(5);
            
            con.close();
            eos.product_populate_flag=true;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
