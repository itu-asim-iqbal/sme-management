package database_finalproject;
import java.awt.CardLayout;
import java.awt.GridLayout;
import java.sql.*;
import java.util.Vector;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;



public class SqlQueriesFunction
{  
    protected static Connection conn;    
    protected static String USER = "";
    protected static String PASS = "";
    protected static String Schema="scott";
    protected static String Exception="";
    
    protected static final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    protected static final String DB_URL = "jdbc:oracle:thin:@localhost:1521:orcl";
    
   public static void setTable(String userName , String password , String tableName , JTable uTable , DefaultTableModel tableModel )
    {
        conn = null;
        Statement stmt = null ;
        try
        {   //System.out.println("there i go");
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL , userName , password) ;
//            conn = DriverManager.getConnection(DB_URL , USER , PASS) ;
            stmt = conn.createStatement();
            alterSession(stmt);
            String sql;
            sql = " Select * from ak_products ";
            sql += tableName;
            ResultSet orderSet = stmt.executeQuery(sql);
            ResultSetMetaData metaData = orderSet.getMetaData();
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            //cbPanel.getContentPane().add(new JScrollPane(cbPanel , JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
            
          for (int i = 1; i <= columnCount; i++) 
            {
                columnNames.add(metaData.getColumnName(i));        
                
                
                
//                cbPanel.setVisible(false);
//                cbPanel.setVisible(true);
                
            }
            Vector<Vector<Object>> data = new Vector<Vector<Object>>();
            while (orderSet.next()) 
            {
                Vector<Object> vector = new Vector<Object>();
                for (int i = 1; i <= columnCount; i++)
                {
                    vector.add(orderSet.getObject(i));
                }
                data.add(vector);
            }
            tableModel.setDataVector(data,columnNames);      
            orderSet.close();
            stmt.close();
            conn.close();
                        
        }
         catch(SQLException se)
        {
            //Handle errors for JDBC
            se.printStackTrace();
        }
        catch(Exception e)
        {
            //Handle errors for Class.forName
            e.printStackTrace();
        }
         finally
        {
//finally block used to close resources
            try
            {
                if(stmt!=null)
                stmt.close();
            }
            catch(SQLException se2)
            {
                
            }// nothing we can do
            try
            {
                if(conn!=null)
                conn.close();
            }
            catch(SQLException se)
            {
                se.printStackTrace();
            }//end finally try
        }
    

    
    }
   
   //////overriding
   public static void setTable(String userName , String password , String tableName , JTable uTable , DefaultTableModel tableModel,String SearchQuery )
    {
        conn = null;
        Statement stmt = null ;
        try
        {   //System.out.println("SearchQuery"+SearchQuery);
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL , userName , password) ;
//            conn = DriverManager.getConnection(DB_URL , USER , PASS) ;
            stmt = conn.createStatement();
            alterSession(stmt);
            String sql;
            sql = SearchQuery;
           // sql += tableName;
            ResultSet orderSet = stmt.executeQuery(sql);
            ResultSetMetaData metaData = orderSet.getMetaData();
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            //cbPanel.getContentPane().add(new JScrollPane(cbPanel , JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
            
          for (int i = 1; i <= columnCount; i++) 
            {
                columnNames.add(metaData.getColumnName(i));        
                
                
                
//                cbPanel.setVisible(false);
//                cbPanel.setVisible(true);
                
            }
            Vector<Vector<Object>> data = new Vector<Vector<Object>>();
            while (orderSet.next()) 
            {
                Vector<Object> vector = new Vector<Object>();
                for (int i = 1; i <= columnCount; i++)
                {
                    vector.add(orderSet.getObject(i));
                }
                data.add(vector);
            }
            tableModel.setDataVector(data,columnNames);      
            orderSet.close();
            stmt.close();
            conn.close();
                        
        }
         catch(SQLException se)
        {
            //Handle errors for JDBC
            se.printStackTrace();
        }
        catch(Exception e)
        {
            //Handle errors for Class.forName
            e.printStackTrace();
        }
         finally
        {
//finally block used to close resources
            try
            {
                if(stmt!=null)
                stmt.close();
            }
            catch(SQLException se2)
            {
                
            }// nothing we can do
            try
            {
                if(conn!=null)
                conn.close();
            }
            catch(SQLException se)
            {
                se.printStackTrace();
            }//end finally try
        }
    

    
    }
   ////////////////////////////////////////////
   
   public static void setTableDelete(String userName , String password , String tableName , JTable uTable , DefaultTableModel tableModel,String DeleteQuery )
    {
        try{  
        //step1 load the driver class  
        Class.forName("oracle.jdbc.driver.OracleDriver");  
  
        //step2 create  the connection object  
        Connection con=DriverManager.getConnection(  
        "jdbc:oracle:thin:@localhost:1521:orcl","system","Farrukh123");  
  
        //step3 create the statement object  
        Statement stmt=con.createStatement();  
        alterSession(stmt);
        //step4 execute query  
        ResultSet rs=stmt.executeQuery(DeleteQuery);  
        //while(rs.next())  
       // System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));  
  
        //step5 close the connection object  
        con.close();  
  
        }catch(Exception e){ System.out.println(e);} 
    }
   
   public static void alterSession(Statement st)
    {
        Exception="";
        try
        {
            st.execute( "alter session set current_schema="+Schema ) ;
            
        }
        catch ( Exception e )
        {
            Exception=e.getMessage();
            e.printStackTrace( ) ;
            throw new RuntimeException("SQL Exception");
        }
    }
    
   
   
}
     /*public static ResultSet getTable ( String userName , String password , String tableName )
    {
        Connection conn = null;
        Statement stmt = null ;
        try
        {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            conn = DriverManager.getConnection(DB_URL , userName , password) ;
//            conn = DriverManager.getConnection(DB_URL , USER , PASS) ;
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM " ;
            sql += tableName;
            ResultSet rs = stmt.executeQuery(sql);
//            rs.close();
//            stmt.close();
//            conn.close();
            return rs;            
        }
         catch(SQLException se)
        {
            //Handle errors for JDBC
            se.printStackTrace();
        }
        catch(Exception e)
        {
            //Handle errors for Class.forName
            e.printStackTrace();
        }
         finally
        {
//finally block used to close resources
            try
            {
                if(stmt!=null)
                stmt.close();
            }
            catch(SQLException se2)
            {
                
            }// nothing we can do
            try
            {
                if(conn!=null)
                conn.close();
            }
            catch(SQLException se)
            {
                se.printStackTrace();
            }//end finally try
        }
        return null;
    }
    
    
    
    
    public static void main (String[] args)
    {
        Connection conn = null ;
        Statement stmt = null ;
        try
        {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL , USER , PASS );
            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT empno, ename, job FROM Emp";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next())
            {
                //Retrieve by column name
                int id = rs.getInt("empno");
                //int age = rs.getInt("ename");
                String name = rs.getString("ename");
                String job = rs.getString("job");
                //Display values
                System.out.print("ID: " + id);
                //System.out.print(", Age: " + age);
                System.out.print(", First: " + name);
                System.out.println(", Last: " + job);
            }            
            rs.close();
            stmt.close();
            conn.close();
        }
        catch(SQLException se)
        {
            //Handle errors for JDBC
            se.printStackTrace();
        }
        catch(Exception e)
        {
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        finally
        {
//finally block used to close resources
            try
            {
                if(stmt!=null)
                stmt.close();
            }
            catch(SQLException se2)
            {
                
            }// nothing we can do
            try
            {
                if(conn!=null)
                conn.close();
            }
            catch(SQLException se)
            {
                se.printStackTrace();
            }//end finally try
        }
        System.out.println("GoodBye!");                
    }
*/

