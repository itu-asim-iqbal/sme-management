package database_finalproject;

import java.awt.CardLayout;
import java.awt.GridLayout;
import java.sql.*;
import java.util.Vector;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;



public class Connect
{  
    protected static Connection conn;    
    protected static String USER = "";
    protected static String PASS = "";
    protected static String Exception="";
    protected static String Schema="scott";
    protected static final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    protected static final String DB_URL = "jdbc:oracle:thin:@localhost:1521:orcl";
    
  
    public static void setTable(String userName , String password , String tableName , JTable uTable , DefaultTableModel tableModel )
    {
        conn = null;
        Statement stmt = null ;
        try
        {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL , userName , password) ;
//            conn = DriverManager.getConnection(DB_URL , USER , PASS) ;
            stmt = conn.createStatement();
             alterSession(stmt);
            String sql;
            sql = "SELECT * FROM " ;
            sql += tableName;
            ResultSet orderSet = stmt.executeQuery(sql);
            ResultSetMetaData metaData = orderSet.getMetaData();
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            //cbPanel.getContentPane().add(new JScrollPane(cbPanel , JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
            
          for (int i = 1; i <= columnCount; i++) 
            {
                columnNames.add(metaData.getColumnName(i));        
                
                
                
//                cbPanel.setVisible(false);
//                cbPanel.setVisible(true);
                
            }
            Vector<Vector<Object>> data = new Vector<Vector<Object>>();
            while (orderSet.next()) 
            {
                Vector<Object> vector = new Vector<Object>();
                for (int i = 1; i <= columnCount; i++)
                {
                    vector.add(orderSet.getObject(i));
                }
                data.add(vector);
            }
            tableModel.setDataVector(data,columnNames);      
            orderSet.close();
            stmt.close();
            conn.close();
                        
        }
         catch(SQLException se)
        {
            //Handle errors for JDBC
            se.printStackTrace();
        }
        catch(Exception e)
        {
            //Handle errors for Class.forName
            e.printStackTrace();
        }
         finally
        {
//finally block used to close resources
            try
            {
                if(stmt!=null)
                stmt.close();
            }
            catch(SQLException se2)
            {
                
            }// nothing we can do
            try
            {
                if(conn!=null)
                conn.close();
            }
            catch(SQLException se)
            {
                se.printStackTrace();
            }//end finally try
        }
    
    }
    /**
     *
     * @param userName
     * @param password
     * @param tableName
     * @param uTable
     * @param tableModel
     * @param Query
     */
    public static void writeTable(String userName , String password , String tableName , JTable uTable , DefaultTableModel tableModel, String Query )
    {
        conn = null;
        Statement stmt = null ;
        try
        {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL , userName , password) ;
//            conn = DriverManager.getConnection(DB_URL , USER , PASS) ;
            stmt = conn.createStatement();
             alterSession(stmt);
            String sql;
            sql= Query;
            ResultSet orderSet = stmt.executeQuery(sql);
            ResultSetMetaData metaData = orderSet.getMetaData();
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            //cbPanel.getContentPane().add(new JScrollPane(cbPanel , JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
            
          for (int i = 1; i <= columnCount; i++) 
            {
                columnNames.add(metaData.getColumnName(i));        
                
                
                
//                cbPanel.setVisible(false);
//                cbPanel.setVisible(true);
                
            }
            Vector<Vector<Object>> data = new Vector<Vector<Object>>();
            while (orderSet.next()) 
            {
                Vector<Object> vector = new Vector<Object>();
                for (int i = 1; i <= columnCount; i++)
                {
                    vector.add(orderSet.getObject(i));
                }
                data.add(vector);
            }
            tableModel.setDataVector(data,columnNames);      
            orderSet.close();
            stmt.close();
            conn.close();
                        
        }
         catch(SQLException se)
        {
            //Handle errors for JDBC
            se.printStackTrace();
        }
        catch(Exception e)
        {
            //Handle errors for Class.forName
            e.printStackTrace();
        }
         finally
        {
//finally block used to close resources
            try
            {
                if(stmt!=null)
                stmt.close();
            }
            catch(SQLException se2)
            {
                
            }// nothing we can do
            try
            {
                if(conn!=null)
                conn.close();
            }
            catch(SQLException se)
            {
                se.printStackTrace();
            }//end finally try
        }
    
    }
        
        
     public static void Query(String userName , String password,String Query)
    {
        Connection con=null;
        Statement st=null;
        try
        {
           Class.forName(JDBC_DRIVER);
           con = DriverManager.getConnection(DB_URL , userName , password);
           st=con.createStatement();
            alterSession(st);
           st.executeUpdate(Query);
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,e);
        }
        
        
                 
                    
    }
        
   
   
   public static void alterSession(Statement st)
    {
        Exception="";
        try
        {
            st.execute( "alter session set current_schema="+Schema ) ;
            
        }
        catch ( Exception e )
        {
            Exception=e.getMessage();
            e.printStackTrace( ) ;
            throw new RuntimeException("SQL Exception");
        }
    }
   

   
   
}


