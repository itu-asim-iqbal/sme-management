package database_finalproject;

import java.awt.Toolkit;
import java.awt.event.ActionListener;

public class AK_MS_Lvl1 extends javax.swing.JFrame 
{
    protected Asim_TimerThreadClass timerThread;
    int timeDelay = 1000;
    ActionListener time;
    
    public AK_MS_Lvl1() 
    {
        
        this.setUndecorated(true);
        this.setAlwaysOnTop(true);
        this.setResizable(false);
        this.setVisible(true);
        
        initComponents();
        
        Toolkit tk = Toolkit.getDefaultToolkit();
        int xsize = (int)tk.getScreenSize().getWidth();
        int ysize = (int)tk.getScreenSize().getHeight();
        //bgPanel.setSize ( xsize , ysize ) ;
        this.setSize(xsize , ysize);
        
        timerThread = new Asim_TimerThreadClass(dateLabel , timeLabel);
        timerThread.start();
        
        userLabel.setText ( Asim_users_login.userName ) ;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgPanel = new javax.swing.JPanel();
        topPanel = new javax.swing.JPanel();
        dateLabel = new javax.swing.JLabel();
        timeLabel = new javax.swing.JLabel();
        userLabel = new javax.swing.JLabel();
        headerLabel = new javax.swing.JLabel();
        bottomPanel = new javax.swing.JPanel();
        bottomLeftSpacerPanel = new javax.swing.JPanel();
        bottomRightSpacerPanel = new javax.swing.JPanel();
        signoutButton = new javax.swing.JButton();
        midPanel = new javax.swing.JPanel();
        LU_ORD_Button = new javax.swing.JButton();
        LU_PROD_Button = new javax.swing.JButton();
        LU_INV_Button = new javax.swing.JButton();
        LU_CLT_Button = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        bgPanel = new MyCanvas ( ) ;

        topPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        topPanel.setOpaque(false);

        dateLabel.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        dateLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        dateLabel.setText("DATE");

        timeLabel.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        timeLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        timeLabel.setText("TIME");

        userLabel.setFont(new java.awt.Font("Calibri", 0, 30)); // NOI18N
        userLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        userLabel.setText("USER");

        headerLabel.setFont(new java.awt.Font("Calibri", 0, 48)); // NOI18N
        headerLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        headerLabel.setText("Welcome to Awab Knitwear");

        javax.swing.GroupLayout topPanelLayout = new javax.swing.GroupLayout(topPanel);
        topPanel.setLayout(topPanelLayout);
        topPanelLayout.setHorizontalGroup(
            topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(topPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dateLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                    .addComponent(timeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(headerLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 601, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(userLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                .addContainerGap())
        );
        topPanelLayout.setVerticalGroup(
            topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(topPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(headerLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(topPanelLayout.createSequentialGroup()
                        .addComponent(dateLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(timeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addComponent(userLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        bottomPanel.setOpaque(false);

        bottomLeftSpacerPanel.setOpaque(false);

        javax.swing.GroupLayout bottomLeftSpacerPanelLayout = new javax.swing.GroupLayout(bottomLeftSpacerPanel);
        bottomLeftSpacerPanel.setLayout(bottomLeftSpacerPanelLayout);
        bottomLeftSpacerPanelLayout.setHorizontalGroup(
            bottomLeftSpacerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        bottomLeftSpacerPanelLayout.setVerticalGroup(
            bottomLeftSpacerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        bottomRightSpacerPanel.setOpaque(false);

        javax.swing.GroupLayout bottomRightSpacerPanelLayout = new javax.swing.GroupLayout(bottomRightSpacerPanel);
        bottomRightSpacerPanel.setLayout(bottomRightSpacerPanelLayout);
        bottomRightSpacerPanelLayout.setHorizontalGroup(
            bottomRightSpacerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        bottomRightSpacerPanelLayout.setVerticalGroup(
            bottomRightSpacerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        signoutButton.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        signoutButton.setText("SIGNOUT");
        signoutButton.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        signoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                signoutButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout bottomPanelLayout = new javax.swing.GroupLayout(bottomPanel);
        bottomPanel.setLayout(bottomPanelLayout);
        bottomPanelLayout.setHorizontalGroup(
            bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bottomPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bottomLeftSpacerPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(signoutButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(bottomRightSpacerPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        bottomPanelLayout.setVerticalGroup(
            bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bottomPanelLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(signoutButton, javax.swing.GroupLayout.DEFAULT_SIZE, 56, Short.MAX_VALUE))
            .addGroup(bottomPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bottomLeftSpacerPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(bottomPanelLayout.createSequentialGroup()
                        .addComponent(bottomRightSpacerPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );

        midPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        midPanel.setOpaque(false);
        midPanel.setLayout(new java.awt.GridLayout(2, 2, 75, 75));

        LU_ORD_Button.setFont(new java.awt.Font("Calibri", 0, 36)); // NOI18N
        LU_ORD_Button.setText("Lookup ORDER");
        LU_ORD_Button.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        LU_ORD_Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LU_ORD_ButtonActionPerformed(evt);
            }
        });
        midPanel.add(LU_ORD_Button);

        LU_PROD_Button.setFont(new java.awt.Font("Calibri", 0, 36)); // NOI18N
        LU_PROD_Button.setText("Lookup PRODUCT");
        LU_PROD_Button.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        LU_PROD_Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LU_PROD_ButtonActionPerformed(evt);
            }
        });
        midPanel.add(LU_PROD_Button);

        LU_INV_Button.setFont(new java.awt.Font("Calibri", 0, 36)); // NOI18N
        LU_INV_Button.setText("Lookup INVENTORY");
        LU_INV_Button.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        LU_INV_Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LU_INV_ButtonActionPerformed(evt);
            }
        });
        midPanel.add(LU_INV_Button);

        LU_CLT_Button.setFont(new java.awt.Font("Calibri", 0, 36)); // NOI18N
        LU_CLT_Button.setText("Lookup CLIENT");
        LU_CLT_Button.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        LU_CLT_Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LU_CLT_ButtonActionPerformed(evt);
            }
        });
        midPanel.add(LU_CLT_Button);

        javax.swing.GroupLayout bgPanelLayout = new javax.swing.GroupLayout(bgPanel);
        bgPanel.setLayout(bgPanelLayout);
        bgPanelLayout.setHorizontalGroup(
            bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bgPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(bottomPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(topPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(midPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        bgPanelLayout.setVerticalGroup(
            bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bgPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(topPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(midPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 390, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(bottomPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 927, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(bgPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, 0)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 629, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(bgPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, 0)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void LU_PROD_ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LU_PROD_ButtonActionPerformed
        // TODO add your handling code here:
        Farrukh_LookUpProduct flup = new Farrukh_LookUpProduct ( ) ;
        flup.setVisible ( true ) ;
        timerThread.stop();
        super.dispose();
    }//GEN-LAST:event_LU_PROD_ButtonActionPerformed

    private void LU_CLT_ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LU_CLT_ButtonActionPerformed
        // TODO add your handling code here:
        Awab_Clients_View f= new Awab_Clients_View();
        f.setVisible(true);
        timerThread.stop();
        super.dispose();
    }//GEN-LAST:event_LU_CLT_ButtonActionPerformed

    private void signoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_signoutButtonActionPerformed
        // TODO add your handling code here:
        Asim_Database_FinalProject.qtm.closeDB() ;        
        timerThread.stop();
        Asim_Database_FinalProject.qtm.USER = "" ;
        Asim_Database_FinalProject.qtm.PASS = "" ;
        Asim_users_login ul = new Asim_users_login ( ) ;
        ul.setVisible(true) ;
        super.dispose();
    }//GEN-LAST:event_signoutButtonActionPerformed

    private void LU_ORD_ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LU_ORD_ButtonActionPerformed
        // TODO add your handling code here:
        Asim_LookUpOrder viewOrders = new Asim_LookUpOrder();
        viewOrders.setVisible(true);
        timerThread.stop();
        super.dispose();
    }//GEN-LAST:event_LU_ORD_ButtonActionPerformed

    private void LU_INV_ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LU_INV_ButtonActionPerformed
        // TODO add your handling code here:
        Shehryar_Inventory sin = new Shehryar_Inventory ( "1" ) ;
        sin.setVisible(true);
        timerThread.stop();
        super.dispose();
    }//GEN-LAST:event_LU_INV_ButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton LU_CLT_Button;
    private javax.swing.JButton LU_INV_Button;
    private javax.swing.JButton LU_ORD_Button;
    private javax.swing.JButton LU_PROD_Button;
    private javax.swing.JPanel bgPanel;
    private javax.swing.JPanel bottomLeftSpacerPanel;
    private javax.swing.JPanel bottomPanel;
    private javax.swing.JPanel bottomRightSpacerPanel;
    private javax.swing.JLabel dateLabel;
    private javax.swing.JLabel headerLabel;
    private javax.swing.JPanel midPanel;
    private javax.swing.JButton signoutButton;
    private javax.swing.JLabel timeLabel;
    private javax.swing.JPanel topPanel;
    private javax.swing.JLabel userLabel;
    // End of variables declaration//GEN-END:variables
}
