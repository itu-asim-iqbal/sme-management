package database_finalproject;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JPanel;

public class MyCanvas extends JPanel
{
    public BufferedImage bgImg;
    @Override
    public void paintComponent(Graphics g)
    {
        try 
        {
            bgImg = ImageIO.read(new File("C:\\Users\\MohammadAsim\\Box Sync\\LMS\\2016_Spring_4\\Database Systems\\Project\\Java\\Database_FinalProject\\Database_FinalProject\\src\\database_finalproject\\images\\RTAA8rAyc2.jpg"));
        } 
        catch (IOException e)
        {
            System.out.println("File not loaded") ;
        }
       super.paintComponent(g);
       if ( bgImg != null )
       {
           g.drawImage(bgImg, 0, 0, this); 
       }
       
    } 
}
