package database_finalproject;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import java.util.Date;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;


public class Shehryar_Inventory extends javax.swing.JFrame 
{
    DefaultTableModel tablemodel;  
    protected Asim_TimerThreadClass timerThread;
    int timeDelay = 1000;
    ActionListener time;
    String access_level;
    
     
    
    public Shehryar_Inventory(String acc) 
    { 
        access_level = acc ;
        tablemodel = new DefaultTableModel()
        {
             public boolean isCellEditable(int i, int j)         
             {
                return false;
             }       
        };
        this.setUndecorated(true);
        this.setResizable(false);
        initComponents();
        id_label.setText(Asim_users_login.userName);
        timerThread = new Asim_TimerThreadClass(Date_Label , Time_Label);
        timerThread.start();
        Toolkit tk=Toolkit.getDefaultToolkit();
        int xsize=(int)tk.getScreenSize().getWidth();
        int ysize=(int)tk.getScreenSize().getWidth();
        this.setSize(xsize,ysize);
        
        if(access_level.equals("1"))
        {
             Del_inv_id_label.setVisible(false);
             Del_text.setVisible(false);
             Delete.setVisible(false);
             Style_Label.setVisible(false);
             Style_text.setVisible(false);
             Type_Label.setVisible(false);
             Type_text.setVisible(false);
             cost_Label.setVisible(false);
             Cost_text.setVisible(false);
             Quantity_Label.setVisible(false);
             Quantity_text.setVisible(false);
             Grade_Label.setVisible(false);
             Grade_text.setVisible(false);
             AddItem.setVisible(false);
             INV_ID_Label.setVisible(false);
             item_text.setVisible(false);
             Col_Name_Label.setVisible(false);
             Col_text.setVisible(false);
             New_Val_Label.setVisible(false);
             New_text.setVisible(false);
             Update.setVisible(false);
             
        }
        
        if(access_level.equals("2"))
        {
             Del_inv_id_label.setVisible(false);
             Del_text.setVisible(false);
             Delete.setVisible(false);
             INV_ID_Label.setVisible(false);
             item_text.setVisible(false);
             Col_Name_Label.setVisible(false);
             Col_text.setVisible(false);
             New_Val_Label.setVisible(false);
             New_text.setVisible(false);
             Update.setVisible(false);
        }
        
         test.setTable("Inventory", Inv_Table, tablemodel);
    
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        bgPanel = new javax.swing.JPanel();
        Home = new javax.swing.JButton();
        Delete = new javax.swing.JButton();
        Search = new javax.swing.JButton();
        Search_Text = new javax.swing.JTextField();
        Look_Inven = new javax.swing.JButton();
        AddItem = new javax.swing.JButton();
        Style_text = new javax.swing.JTextField();
        Style_Label = new javax.swing.JLabel();
        cost_Label = new javax.swing.JLabel();
        Type_Label = new javax.swing.JLabel();
        Grade_Label = new javax.swing.JLabel();
        Quantity_Label = new javax.swing.JLabel();
        Cost_text = new javax.swing.JTextField();
        Type_text = new javax.swing.JTextField();
        Grade_text = new javax.swing.JTextField();
        Quantity_text = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        Inv_Table = new javax.swing.JTable();
        item_text = new javax.swing.JTextField();
        INV_ID_Label = new javax.swing.JLabel();
        Col_text = new javax.swing.JTextField();
        Col_Name_Label = new javax.swing.JLabel();
        Update = new javax.swing.JButton();
        Del_inv_id_label = new javax.swing.JLabel();
        Del_text = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        New_Val_Label = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        id_label = new javax.swing.JLabel();
        Time_Label = new javax.swing.JLabel();
        Date_Label = new javax.swing.JLabel();
        New_text = new javax.swing.JTextField();
        Drop_Search = new javax.swing.JComboBox();

        jTextField1.setText("jTextField1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        bgPanel = new MyCanvas ( ) ;

        Home.setText("Home");
        Home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HomeActionPerformed(evt);
            }
        });

        Delete.setText("Delete An Item");
        Delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DeleteActionPerformed(evt);
            }
        });

        Search.setText("Search");
        Search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SearchActionPerformed(evt);
            }
        });

        Look_Inven.setText("Look Up Inventory");
        Look_Inven.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Look_InvenActionPerformed(evt);
            }
        });

        AddItem.setText("Add Item");
        AddItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddItemActionPerformed(evt);
            }
        });

        Style_Label.setText("Style:");

        cost_Label.setText("Cost:");

        Type_Label.setText("Type:");

        Grade_Label.setText("Grade:");

        Quantity_Label.setText("Quantity:");

        Cost_text.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Cost_textActionPerformed(evt);
            }
        });

        Type_text.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Type_textActionPerformed(evt);
            }
        });

        Grade_text.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Grade_textActionPerformed(evt);
            }
        });

        Quantity_text.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Quantity_textActionPerformed(evt);
            }
        });

        Inv_Table.setAutoCreateRowSorter(true);
        Inv_Table.setModel(tablemodel);
        Inv_Table.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        Inv_Table.setColumnSelectionAllowed(true);
        Inv_Table.setColumnSelectionAllowed(false);
        Inv_Table.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(Inv_Table);

        item_text.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                item_textActionPerformed(evt);
            }
        });

        INV_ID_Label.setText("Inv_id");

        Col_Name_Label.setText("Column Name:");

        Update.setText("Update Inventory");
        Update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UpdateActionPerformed(evt);
            }
        });

        Del_inv_id_label.setText("INV_ID");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Inventory");
        jLabel1.setAlignmentX(1.0F);
        jLabel1.setPreferredSize(new java.awt.Dimension(40, 15));

        New_Val_Label.setText("New Value:");

        jLabel13.setText("USER ID:");

        Time_Label.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        Date_Label.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        New_text.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                New_textActionPerformed(evt);
            }
        });

        Drop_Search.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Type_Inv", "Cost_Inv", "Quantity_Inv", "Grade_Inv", "Inv_Id" }));
        Drop_Search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Drop_SearchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout bgPanelLayout = new javax.swing.GroupLayout(bgPanel);
        bgPanel.setLayout(bgPanelLayout);
        bgPanelLayout.setHorizontalGroup(
            bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bgPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(bgPanelLayout.createSequentialGroup()
                            .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(Look_Inven)
                                .addComponent(Del_inv_id_label, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(Style_Label, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cost_Label, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(Type_Label, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(Grade_Label, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE))
                                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(INV_ID_Label, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(New_Val_Label, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE)))
                            .addGap(39, 39, 39))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bgPanelLayout.createSequentialGroup()
                            .addComponent(Date_Label, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                    .addGroup(bgPanelLayout.createSequentialGroup()
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Quantity_Label, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Col_Name_Label, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bgPanelLayout.createSequentialGroup()
                        .addComponent(Drop_Search, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(87, 87, 87))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bgPanelLayout.createSequentialGroup()
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Time_Label, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bgPanelLayout.createSequentialGroup()
                                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(Grade_text, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Update, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(item_text, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                                        .addComponent(Col_text)
                                        .addComponent(New_text))
                                    .addComponent(Del_text, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Delete, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Style_text, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Type_text, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Cost_text, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(AddItem, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Quantity_text, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(117, 117, 117)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bgPanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())
                    .addGroup(bgPanelLayout.createSequentialGroup()
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(bgPanelLayout.createSequentialGroup()
                                .addComponent(Search_Text, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                                .addComponent(Search, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 141, Short.MAX_VALUE)
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(id_label, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Home, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE))
                        .addGap(36, 36, 36))))
        );
        bgPanelLayout.setVerticalGroup(
            bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bgPanelLayout.createSequentialGroup()
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bgPanelLayout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(id_label, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(bgPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(bgPanelLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Time_Label, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Date_Label, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(Search_Text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Search))
                    .addComponent(Home)
                    .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(Look_Inven)
                        .addComponent(Drop_Search, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bgPanelLayout.createSequentialGroup()
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Del_inv_id_label, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Del_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(4, 4, 4)
                        .addComponent(Delete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Quantity_Label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Quantity_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Type_text, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Type_Label, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cost_Label, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Cost_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Style_Label, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Style_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Grade_Label, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Grade_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(AddItem)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(item_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(INV_ID_Label, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Col_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Col_Name_Label, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(New_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(New_Val_Label, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Update)
                        .addGap(0, 18, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1146, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(bgPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, 0)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 466, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(bgPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, 0)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Cost_textActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Cost_textActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Cost_textActionPerformed

    private void item_textActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_item_textActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_item_textActionPerformed

    private void HomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HomeActionPerformed
        
        if ( Asim_users_login.accessLevel.equals ( "1" ) )
            {
                 AK_MS_Lvl1 main1 = new AK_MS_Lvl1 ( ) ;
                 main1.setVisible(true);
                 timerThread.stop();
                 super.dispose( ) ;
            }
            else if ( Asim_users_login.accessLevel.equals ( "2" ) )
            {
                 AK_MS_Lvl2 main2 = new AK_MS_Lvl2 ( ) ;
                 main2.setVisible(true);
                 timerThread.stop();
                 super.dispose( ) ;
            }

            else if ( Asim_users_login.accessLevel.equals ( "3" ) ) 
            {
                 AK_MS_Lvl3 main3 = new AK_MS_Lvl3 ( ) ;
                 main3.setVisible(true);
                 timerThread.stop();
                 super.dispose( ) ;
            } 
    }//GEN-LAST:event_HomeActionPerformed

    private void Grade_textActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Grade_textActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Grade_textActionPerformed

    
    private void Look_InvenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Look_InvenActionPerformed
        test.setTable("Inventory", Inv_Table, tablemodel);
    }//GEN-LAST:event_Look_InvenActionPerformed

    private void SearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SearchActionPerformed
        String text = Search_Text.getText();
        String text1 = Drop_Search.getSelectedItem().toString();
        test.SearchTable("Inventory", Inv_Table, tablemodel, text1 ,text);
        Search_Text.setText("");
    }//GEN-LAST:event_SearchActionPerformed

    private void DeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DeleteActionPerformed
        
        if (JOptionPane.showConfirmDialog( null , "Are you sure you want to Delete This Item" , "Confirm" , JOptionPane.YES_NO_OPTION , JOptionPane.QUESTION_MESSAGE ) == JOptionPane.YES_OPTION)
        {
            String text = Del_text.getText();
            test.DeleteTable("Inventory", Inv_Table, tablemodel, text);
            test.setTable("Inventory", Inv_Table, tablemodel);
            Del_text.setText("");
        }
    }//GEN-LAST:event_DeleteActionPerformed

    private void AddItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddItemActionPerformed
        if (JOptionPane.showConfirmDialog( null , "Are you sure you want to Add An Item" , "Confirm" , JOptionPane.YES_NO_OPTION , JOptionPane.QUESTION_MESSAGE ) == JOptionPane.YES_OPTION)
       {    
            String text = Style_text.getText();
            String text1 = Type_text.getText();
            String text2 = Cost_text.getText();
            String text3 = Quantity_text.getText();
            String text4 = Grade_text.getText();
            test.Add("Inventory", Inv_Table, tablemodel, text, text1, text2, text3, text4);
            test.setTable("Inventory", Inv_Table, tablemodel);
            Style_text.setText("");
            Type_text.setText("");
            Cost_text.setText("");
            Quantity_text.setText("");
            Grade_text.setText("");
        }
    }//GEN-LAST:event_AddItemActionPerformed

    private void UpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UpdateActionPerformed
        
        if (JOptionPane.showConfirmDialog( null , "Are you sure you want to Update The Inventory" , "Confirm" , JOptionPane.YES_NO_OPTION , JOptionPane.QUESTION_MESSAGE ) == JOptionPane.YES_OPTION)
        {
            String text = item_text.getText();
            String text1 = Col_text.getText();
            String text2 = New_text.getText();
            test.EditTable("Inventory", Inv_Table, tablemodel, text, text1, text2);
            test.setTable("Inventory", Inv_Table, tablemodel);
            item_text.setText("");
            Col_text.setText("");
            New_text.setText("");
        }

        
    }//GEN-LAST:event_UpdateActionPerformed

    private void New_textActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_New_textActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_New_textActionPerformed

    private void Drop_SearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Drop_SearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Drop_SearchActionPerformed

    private void Type_textActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Type_textActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Type_textActionPerformed

    private void Quantity_textActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Quantity_textActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Quantity_textActionPerformed
   /* public static void main(String args[]) 
    {
        java.awt.EventQueue.invokeLater(new Runnable() 
        {
            public void run() 
            {
                new Shehryar_Inventory().setVisible(true);
            }
        });
    }*/

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AddItem;
    private javax.swing.JLabel Col_Name_Label;
    private javax.swing.JTextField Col_text;
    private javax.swing.JTextField Cost_text;
    private javax.swing.JLabel Date_Label;
    private javax.swing.JLabel Del_inv_id_label;
    private javax.swing.JTextField Del_text;
    private javax.swing.JButton Delete;
    private javax.swing.JComboBox Drop_Search;
    private javax.swing.JLabel Grade_Label;
    private javax.swing.JTextField Grade_text;
    private javax.swing.JButton Home;
    private javax.swing.JLabel INV_ID_Label;
    private javax.swing.JTable Inv_Table;
    private javax.swing.JButton Look_Inven;
    private javax.swing.JLabel New_Val_Label;
    private javax.swing.JTextField New_text;
    private javax.swing.JLabel Quantity_Label;
    private javax.swing.JTextField Quantity_text;
    private javax.swing.JButton Search;
    private javax.swing.JTextField Search_Text;
    private javax.swing.JLabel Style_Label;
    private javax.swing.JTextField Style_text;
    private javax.swing.JLabel Time_Label;
    private javax.swing.JLabel Type_Label;
    private javax.swing.JTextField Type_text;
    private javax.swing.JButton Update;
    private javax.swing.JPanel bgPanel;
    private javax.swing.JLabel cost_Label;
    private javax.swing.JLabel id_label;
    private javax.swing.JTextField item_text;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
