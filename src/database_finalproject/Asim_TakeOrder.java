/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database_finalproject;

import com.toedter.calendar.JTextFieldDateEditor;
import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFormattedTextField;
import javax.swing.JFormattedTextField.AbstractFormatterFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.NumberFormatter;


/**
 *
 * @author MohammadAsim
 */
public class Asim_TakeOrder extends javax.swing.JFrame {
    int prodID ;
    int clientID ;  
    
    
    float taxAmount = 0 ;
    float prodPrice = 0 ;
    int prodQty = 0 ;
    float orderDisc = 0 ;
    
    float totalPrice = 0 ;
    float finalPrice = 0 ;
    
    String orderRecDate ;
    String orderDelDate;
    
    String deliveryAddress ;
    
    
    //JFormattedTextField tkOrd_Field_Qty ;
    Asim_TimerThreadClass timerThread;
    
    BufferedImage bgImg;
    
    public Asim_TakeOrder() 
    {
        this.setUndecorated(true) ;
//        this.setAlwaysOnTop(true) ;
        this.setResizable(false) ;
        this.setVisible(true) ;
        
//        AbstractFormatterFactory ;
              
        
//        NumberFormat format = NumberFormat.getInstance();
//        NumberFormatter formatter = new NumberFormatter(format);
//        formatter.setValueClass(Integer.class);
//        formatter.setMinimum(0);
//        formatter.setMaximum(Integer.MAX_VALUE);
//        formatter.setAllowsInvalid(false);
//        // If you want the value to be committed on each keystroke instead of focus lost
//        formatter.setCommitsOnValidEdit(true);
//        tkOrd_Field_Qty = new JFormattedTextField(formatter);
//        tkOrd_Field_Qty.setVisible(true) ;
//        jPanel1.add ( tkOrd_Field_Qty ) ;

         
        initComponents() ;
//        this.setContentPane(new MyCanvas()); 
        

        
        Toolkit tk = Toolkit.getDefaultToolkit() ;
        int xsize = (int)tk.getScreenSize().getWidth() ;
        int ysize = (int)tk.getScreenSize().getHeight() ;
        this.setSize(xsize , ysize) ;
        timerThread = new Asim_TimerThreadClass(tkOrd_DateLabel1 , tkOrd_TimeLabel2) ;
        timerThread.start() ;  
        
        //tkOrd_DateChooser_Deliv
        JTextFieldDateEditor editor = (JTextFieldDateEditor) tkOrd_DateChooser_Deliv.getDateEditor();
        editor.setEditable(false);
        
        tkOrd_Field_Qty.getDocument().addDocumentListener(new DocumentListener() 
        {
            public void changedUpdate(DocumentEvent e)
            {
               
            }
            public void removeUpdate(DocumentEvent e)
            {
                
            }
            public void insertUpdate(DocumentEvent e)
            {
               String tempAmount =  tkOrd_Field_Qty.getText ( ).trim( )  ;
               if ( ! tempAmount.equals ( "" ) )
                {
                    calcValues ( ) ;
                }
               else
                {
                    tkOrd_Label_AmountVal.setText("0.00") ;
                    tkOrd_Label_TaxVal.setText("0.00") ;
                }
            }            
        });
        
        
       tkOrd_Field_Discount.getDocument().addDocumentListener(new DocumentListener() 
        {
            public void changedUpdate(DocumentEvent e)
            {
               
            }
            public void removeUpdate(DocumentEvent e)
            {
                
            }
            public void insertUpdate(DocumentEvent e)
            {
               String tempDisc =  tkOrd_Field_Discount.getText ( ).trim( )  ;               
               if ( ! tempDisc.equals ( "" )  )
                { 
                    calcValues ( ) ;
                }
            }            
        });
       
        String prodQ = "SELECT * FROM AK_PRODUCTS" ;
        String clientQ = "SELECT * FROM CLIENTS" ;
        String prodIDLabel = "PROD_ID" ;
        String prodNameLabel = "PROD_NAME" ;                          // PRODUCT NAME + GRADE
        String prodGradeLabel = "PROD_TYPE" ;
        String clientIDLabel = "CLIENT_ID" ;
        String clientNameLabel = "COMPANY" ;
        
        Asim_Database_FinalProject.qtm.setCombo ( prodQ , prodIDLabel , prodNameLabel ,  prodGradeLabel , tkOrd_List_Prod ) ;
        Asim_Database_FinalProject.qtm.setCombo ( clientQ , clientIDLabel , clientNameLabel , "" , tkOrd_List_Client ) ;
        
        
    }
    
    private void calcValues ( )
    {
        String prodCombo = tkOrd_List_Prod.getSelectedItem ( ).toString()  ;
        String [ ] productParts = prodCombo.split("-") ;
        prodID = Integer.parseInt(productParts [ 0 ] ) ;
        String q = "SELECT * FROM AK_PRODUCTS WHERE PROD_ID = " + prodID ;
        String columnName = "PROD_COST" ;
        String tempCost = Asim_Database_FinalProject.qtm.getVal ( q , columnName ) ;
        prodPrice = Float.parseFloat ( tempCost ) ;
        
        String tempDisc = tkOrd_Field_Discount.getText ( ).trim( ) ;
        String tempQty = tkOrd_Field_Qty.getText ( ).trim( ) ;
        if ( ! tempDisc.equals("") )
        {
            orderDisc = Float.parseFloat ( tempDisc ) / 100f ;
        }
        else
        {
            orderDisc = 0 ;
        }
        if ( ! tempQty.equals("") )
        {
            prodQty = Integer.parseInt ( tempQty ) ;
        }
        else
        {
            prodQty = 0 ;
        }
        float netTotal = prodPrice * prodQty ;
        float discAmount = prodPrice * prodQty * orderDisc ;
        taxAmount =  (netTotal - discAmount) * 0.15f ;
        finalPrice = netTotal - discAmount + taxAmount ;
        tkOrd_Label_AmountVal.setText ( "" + (float) ( netTotal ) ) ;
        tkOrd_Label_DiscountVal.setText ( "" + ( float ) ( discAmount ) ) ;
        tkOrd_Label_TaxVal.setText ( "" + ( float ) ( taxAmount )  ) ;
        tkOrd_Label_TotalVal.setText ( "" + ( float ) (finalPrice ) ) ;        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel_MainFrame = new javax.swing.JPanel();
        tkOrd_ScreenNameLabel = new javax.swing.JLabel();
        tkOrd_UserNameLabel = new javax.swing.JLabel();
        tkOrd_DateLabel1 = new javax.swing.JLabel();
        tkOrd_TimeLabel2 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        tkOrd_Label_Prod = new javax.swing.JLabel();
        tkOrd_List_Prod = new javax.swing.JComboBox<>();
        tkOrd_Label_Qty = new javax.swing.JLabel();
        tkOrd_Label_Client = new javax.swing.JLabel();
        tkOrd_List_Client = new javax.swing.JComboBox<>();
        tkOrd_Label_Deliv = new javax.swing.JLabel();
        tkOrd_DateChooser_Deliv = new com.toedter.calendar.JDateChooser();
        tkOrd_Label_Dest = new javax.swing.JLabel();
        tkOrd_Field_Dest = new javax.swing.JTextField();
        tkOrd_Label_DiscM = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        tkOrd_Label_Amount = new javax.swing.JLabel();
        tkOrd_Label_AmountVal = new javax.swing.JLabel();
        tkOrd_Label_Tax = new javax.swing.JLabel();
        tkOrd_Label_TaxVal = new javax.swing.JLabel();
        tkOrd_Label_Discount = new javax.swing.JLabel();
        tkOrd_Label_DiscountVal = new javax.swing.JLabel();
        tkOrd_Label_Total = new javax.swing.JLabel();
        tkOrd_Label_TotalVal = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        tkOrd_Field_Qty = new javax.swing.JFormattedTextField();
        tkOrd_Field_Discount = new javax.swing.JFormattedTextField();
        tkOrd_Button_Accept_Bottom = new javax.swing.JButton();
        tkOrd_Button_Cancel_Bottom = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        panel_MainFrame = new MyCanvas ( ) ;
        panel_MainFrame.setName("panel_MainFrame"); // NOI18N

        tkOrd_ScreenNameLabel.setFont(new java.awt.Font("Calibri", 1, 36)); // NOI18N
        tkOrd_ScreenNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tkOrd_ScreenNameLabel.setText("TAKE ORDER");

        tkOrd_UserNameLabel.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_UserNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tkOrd_UserNameLabel.setText("User ID");
        tkOrd_UserNameLabel.setText(Asim_users_login.userName);

        tkOrd_DateLabel1.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_DateLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tkOrd_DateLabel1.setText("DATE");

        tkOrd_TimeLabel2.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_TimeLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tkOrd_TimeLabel2.setText("TIME");

        jSeparator2.setPreferredSize(new java.awt.Dimension(800, 10));

        tkOrd_Label_Prod.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_Label_Prod.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tkOrd_Label_Prod.setText("Product");
        tkOrd_Label_Prod.setPreferredSize(new java.awt.Dimension(200, 50));

        tkOrd_List_Prod.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_List_Prod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tkOrd_List_ProdActionPerformed(evt);
            }
        });

        tkOrd_Label_Qty.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_Label_Qty.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tkOrd_Label_Qty.setText("Quantity");
        tkOrd_Label_Qty.setPreferredSize(new java.awt.Dimension(200, 50));

        tkOrd_Label_Client.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_Label_Client.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tkOrd_Label_Client.setText("Client");
        tkOrd_Label_Client.setPreferredSize(new java.awt.Dimension(200, 50));

        tkOrd_List_Client.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_List_Client.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tkOrd_List_ClientActionPerformed(evt);
            }
        });

        tkOrd_Label_Deliv.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_Label_Deliv.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tkOrd_Label_Deliv.setText("Delivery Date");
        tkOrd_Label_Deliv.setPreferredSize(new java.awt.Dimension(200, 50));

        tkOrd_DateChooser_Deliv.setDateFormatString("dd-MM-yyyy");
        tkOrd_DateChooser_Deliv.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_DateChooser_Deliv.setMinSelectableDate(new Date());
        tkOrd_DateChooser_Deliv.setPreferredSize(new java.awt.Dimension(300, 50));

        tkOrd_Label_Dest.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_Label_Dest.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tkOrd_Label_Dest.setText("Destination");
        tkOrd_Label_Dest.setPreferredSize(new java.awt.Dimension(200, 50));

        tkOrd_Field_Dest.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_Field_Dest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tkOrd_Field_DestActionPerformed(evt);
            }
        });

        tkOrd_Label_DiscM.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_Label_DiscM.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tkOrd_Label_DiscM.setText("Discount");
        tkOrd_Label_DiscM.setPreferredSize(new java.awt.Dimension(200, 50));

        jSeparator1.setPreferredSize(new java.awt.Dimension(800, 10));

        tkOrd_Label_Amount.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tkOrd_Label_Amount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        tkOrd_Label_Amount.setText("Amount ( Rs. )");
        tkOrd_Label_Amount.setToolTipText("");
        tkOrd_Label_Amount.setPreferredSize(new java.awt.Dimension(300, 20));

        tkOrd_Label_AmountVal.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tkOrd_Label_AmountVal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        tkOrd_Label_AmountVal.setText("0.00");
        tkOrd_Label_AmountVal.setToolTipText("");
        tkOrd_Label_AmountVal.setPreferredSize(new java.awt.Dimension(300, 20));

        tkOrd_Label_Tax.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tkOrd_Label_Tax.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        tkOrd_Label_Tax.setText("Tax ( 15 % )");
        tkOrd_Label_Tax.setToolTipText("");
        tkOrd_Label_Tax.setPreferredSize(new java.awt.Dimension(300, 20));

        tkOrd_Label_TaxVal.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tkOrd_Label_TaxVal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        tkOrd_Label_TaxVal.setText("0.00");
        tkOrd_Label_TaxVal.setToolTipText("");
        tkOrd_Label_TaxVal.setPreferredSize(new java.awt.Dimension(300, 20));

        tkOrd_Label_Discount.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tkOrd_Label_Discount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        tkOrd_Label_Discount.setText("Discount ( % )");
        tkOrd_Label_Discount.setToolTipText("");
        tkOrd_Label_Discount.setPreferredSize(new java.awt.Dimension(300, 20));

        tkOrd_Label_DiscountVal.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tkOrd_Label_DiscountVal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        tkOrd_Label_DiscountVal.setText("0.00");
        tkOrd_Label_DiscountVal.setToolTipText("");
        tkOrd_Label_DiscountVal.setPreferredSize(new java.awt.Dimension(300, 20));

        tkOrd_Label_Total.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        tkOrd_Label_Total.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        tkOrd_Label_Total.setText("Total ( Rs. )");
        tkOrd_Label_Total.setToolTipText("");
        tkOrd_Label_Total.setPreferredSize(new java.awt.Dimension(300, 20));

        tkOrd_Label_TotalVal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        tkOrd_Label_TotalVal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        tkOrd_Label_TotalVal.setText("0.00");
        tkOrd_Label_TotalVal.setToolTipText("");
        tkOrd_Label_TotalVal.setPreferredSize(new java.awt.Dimension(300, 20));

        jSeparator3.setPreferredSize(new java.awt.Dimension(800, 10));

        try {
            tkOrd_Field_Qty.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#######")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        tkOrd_Field_Qty.setFocusLostBehavior(javax.swing.JFormattedTextField.PERSIST);
        tkOrd_Field_Qty.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_Field_Qty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tkOrd_Field_QtyActionPerformed(evt);
            }
        });

        try {
            tkOrd_Field_Discount.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        tkOrd_Field_Discount.setText("0 ");
        tkOrd_Field_Discount.setFocusLostBehavior(javax.swing.JFormattedTextField.PERSIST);
        tkOrd_Field_Discount.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_Field_Discount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tkOrd_Field_DiscountActionPerformed(evt);
            }
        });

        tkOrd_Button_Accept_Bottom.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_Button_Accept_Bottom.setText("Accept");
        tkOrd_Button_Accept_Bottom.setPreferredSize(new java.awt.Dimension(140, 40));
        tkOrd_Button_Accept_Bottom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tkOrd_Button_Accept_BottomActionPerformed(evt);
            }
        });

        tkOrd_Button_Cancel_Bottom.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        tkOrd_Button_Cancel_Bottom.setText("Cancel");
        tkOrd_Button_Cancel_Bottom.setPreferredSize(new java.awt.Dimension(140, 40));
        tkOrd_Button_Cancel_Bottom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tkOrd_Button_Cancel_BottomActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel_MainFrameLayout = new javax.swing.GroupLayout(panel_MainFrame);
        panel_MainFrame.setLayout(panel_MainFrameLayout);
        panel_MainFrameLayout.setHorizontalGroup(
            panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_MainFrameLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_MainFrameLayout.createSequentialGroup()
                        .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(panel_MainFrameLayout.createSequentialGroup()
                        .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tkOrd_DateLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tkOrd_TimeLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tkOrd_ScreenNameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tkOrd_UserNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(panel_MainFrameLayout.createSequentialGroup()
                        .addComponent(jSeparator3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_MainFrameLayout.createSequentialGroup()
                        .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(tkOrd_Label_Amount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(tkOrd_Label_Tax, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(tkOrd_Label_Discount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(tkOrd_Label_Total, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tkOrd_Label_TaxVal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(tkOrd_Label_AmountVal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(tkOrd_Label_DiscountVal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tkOrd_Label_TotalVal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_MainFrameLayout.createSequentialGroup()
                        .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jSeparator2, javax.swing.GroupLayout.DEFAULT_SIZE, 1004, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panel_MainFrameLayout.createSequentialGroup()
                                .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_MainFrameLayout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(tkOrd_Label_DiscM, javax.swing.GroupLayout.PREFERRED_SIZE, 390, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(tkOrd_Label_Prod, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                                        .addComponent(tkOrd_Label_Qty, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(tkOrd_Label_Client, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(tkOrd_Label_Deliv, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(tkOrd_Label_Dest, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 390, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(tkOrd_Field_Discount, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(tkOrd_List_Client, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(tkOrd_DateChooser_Deliv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(tkOrd_Field_Dest, javax.swing.GroupLayout.Alignment.TRAILING))
                                    .addComponent(tkOrd_List_Prod, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(tkOrd_Field_Qty))))
                        .addGap(16, 16, 16))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_MainFrameLayout.createSequentialGroup()
                        .addComponent(tkOrd_Button_Accept_Bottom, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(tkOrd_Button_Cancel_Bottom, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        panel_MainFrameLayout.setVerticalGroup(
            panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_MainFrameLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_MainFrameLayout.createSequentialGroup()
                        .addComponent(tkOrd_DateLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tkOrd_TimeLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(tkOrd_UserNameLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(tkOrd_ScreenNameLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tkOrd_List_Prod)
                    .addComponent(tkOrd_Label_Prod, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tkOrd_Field_Qty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tkOrd_Label_Qty, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tkOrd_List_Client, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tkOrd_Label_Client, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tkOrd_DateChooser_Deliv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tkOrd_Label_Deliv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tkOrd_Field_Dest, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tkOrd_Label_Dest, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tkOrd_Field_Discount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tkOrd_Label_DiscM, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(20, 20, 20)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tkOrd_Label_AmountVal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tkOrd_Label_Amount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tkOrd_Label_Discount, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tkOrd_Label_DiscountVal, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tkOrd_Label_Tax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tkOrd_Label_TaxVal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tkOrd_Label_TotalVal, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tkOrd_Label_Total, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_MainFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tkOrd_Button_Cancel_Bottom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tkOrd_Button_Accept_Bottom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel_MainFrame, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel_MainFrame, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tkOrd_List_ProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tkOrd_List_ProdActionPerformed
        // TODO add your handling code here:
        String tempAmount =  tkOrd_Field_Qty.getText ( ).trim( )  ;
        if ( ! tempAmount.equals ( "" ) )
         {
            calcValues ( ) ;
         }
    }//GEN-LAST:event_tkOrd_List_ProdActionPerformed

    private void tkOrd_Field_DestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tkOrd_Field_DestActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tkOrd_Field_DestActionPerformed

    private void tkOrd_List_ClientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tkOrd_List_ClientActionPerformed
        // TODO add your handling code here:
        String clientCombo = tkOrd_List_Client.getSelectedItem ( ).toString()  ;
        String [ ] clientParts = clientCombo.split ( "-" ) ;
        clientID = Integer.parseInt( clientParts [ 0 ] ) ;
    }//GEN-LAST:event_tkOrd_List_ClientActionPerformed

    private void tkOrd_Button_Accept_BottomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tkOrd_Button_Accept_BottomActionPerformed
        
        DateFormat dateFormat = new SimpleDateFormat(" dd-MM-yyyy") ;
        Date currentDate = new Date( ) ;
        orderRecDate = dateFormat.format( currentDate ) ;
        Date deliveryDate = tkOrd_DateChooser_Deliv.getDate( ) ;
        try
        {
            orderDelDate = dateFormat.format( deliveryDate ) ;
        }
        catch (NullPointerException ne) 
        {
            JOptionPane.showMessageDialog(null, "Please select a valid delivery date");//, "ERROR: " , JOptionPane.INFORMATION_MESSAGE
            return ;
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        if (  tkOrd_Field_Qty.getText ( ).trim( ).equals ( "" ) ) 
        {
             JOptionPane.showMessageDialog(null, "Please enter a valid quantity");//, "ERROR: " , JOptionPane.INFORMATION_MESSAGE
            return ;
        }
        
        deliveryAddress = tkOrd_Field_Dest.getText ( ) ;
        if ( deliveryAddress.length( ) < 5 )
        {
            JOptionPane.showMessageDialog(null, "Please enter a valid delivery address");//, "ERROR: " , JOptionPane.INFORMATION_MESSAGE
            return ;
        }
        
        if ( ! tkOrd_Field_Discount.getText ( ).trim( ).equals ( "" ) &&  ! tkOrd_Field_Discount.getText ( ).trim( ).equals ( "0" ) )
        {
            if (JOptionPane.showConfirmDialog(null, "Confirm Discount " + Integer.parseInt ( tkOrd_Field_Discount.getText ( ).trim( ) ) + "%" , "Confirm",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
            {

            }  
            else
            {
                return ;
            }
        }
        
        
        
        if (JOptionPane.showConfirmDialog(null, "Are you sure you want to ADD the ORDER?", "Confirm",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
        {
            String orderQuery = "INSERT INTO AK_ORDER ( PROD_ID , CLIENT_ID , ORDER_QTY , ORDER_DATE , DELIV_DATE , ORDER_DEST , ORDER_TAX , ORDER_PRICE , ORDER_STATUS) " ;
            orderQuery += "VALUES ( " ;
            orderQuery += prodID + " , " ;
            orderQuery += clientID + " , " ;
            orderQuery += prodQty + " , " ;
            orderQuery += "TO_DATE ( '"+ orderRecDate + "' , 'DD-MM-YYYY' ) , " ;
            orderQuery += "TO_DATE ( '"+ orderDelDate + "' , 'DD-MM-YYYY' ) , " ;
            orderQuery += "'" + deliveryAddress + "' , " ;
            orderQuery += taxAmount + " , " ;
            orderQuery += finalPrice + " , " ;
            orderQuery += "'Approval Pending' )" ;
            
            Asim_Database_FinalProject.qtm.insertQuery( orderQuery ) ;
            Asim_Database_FinalProject.qtm.insertQuery ( "COMMIT" ) ;
            
            if ( Asim_users_login.accessLevel.equals ( "1" ) )
            {
                 AK_MS_Lvl1 main1 = new AK_MS_Lvl1 ( ) ;
                 main1.setVisible(true);
                 timerThread.stop();
                 super.dispose( ) ;
            }
            else if ( Asim_users_login.accessLevel.equals ( "2" ) )
            {
                 AK_MS_Lvl2 main2 = new AK_MS_Lvl2 ( ) ;
                 main2.setVisible(true);
                 timerThread.stop();
                 super.dispose( ) ;
            }

            else if ( Asim_users_login.accessLevel.equals ( "3" ) ) 
            {
                 AK_MS_Lvl3 main3 = new AK_MS_Lvl3 ( ) ;
                 main3.setVisible(true);
                 timerThread.stop();
                 super.dispose( ) ;
            }      
        }
        else
        {
            return ;
        }

    }//GEN-LAST:event_tkOrd_Button_Accept_BottomActionPerformed

    private void tkOrd_Button_Cancel_BottomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tkOrd_Button_Cancel_BottomActionPerformed
        //System.out.println( tkOrd_DateChooser_Deliv.getDate ( ) . toString( ) ) ;
        if (JOptionPane.showConfirmDialog(null, "Are you sure you want to CANCEL the ORDER?", "Confirm",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
        {
            if ( Asim_users_login.accessLevel.equals ( "1" ) )
            {
                 AK_MS_Lvl1 main1 = new AK_MS_Lvl1 ( ) ;
                 main1.setVisible(true);
                 timerThread.stop();
                 super.dispose( ) ;
            }
            else if ( Asim_users_login.accessLevel.equals ( "2" ) )
            {
                 AK_MS_Lvl2 main2 = new AK_MS_Lvl2 ( ) ;
                 main2.setVisible(true);
                 timerThread.stop();
                 super.dispose( ) ;
            }

            else if ( Asim_users_login.accessLevel.equals ( "3" ) ) 
            {
                 AK_MS_Lvl3 main3 = new AK_MS_Lvl3 ( ) ;
                 main3.setVisible(true);
                 timerThread.stop();
                 super.dispose( ) ;
            }      
        }
    }//GEN-LAST:event_tkOrd_Button_Cancel_BottomActionPerformed

    private void tkOrd_Field_QtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tkOrd_Field_QtyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tkOrd_Field_QtyActionPerformed

    private void tkOrd_Field_DiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tkOrd_Field_DiscountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tkOrd_Field_DiscountActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(Asim_TakeOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(Asim_TakeOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(Asim_TakeOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(Asim_TakeOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new Asim_TakeOrder().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JPanel panel_MainFrame;
    private javax.swing.JButton tkOrd_Button_Accept_Bottom;
    private javax.swing.JButton tkOrd_Button_Cancel_Bottom;
    private com.toedter.calendar.JDateChooser tkOrd_DateChooser_Deliv;
    private javax.swing.JLabel tkOrd_DateLabel1;
    private javax.swing.JTextField tkOrd_Field_Dest;
    private javax.swing.JFormattedTextField tkOrd_Field_Discount;
    private javax.swing.JFormattedTextField tkOrd_Field_Qty;
    private javax.swing.JLabel tkOrd_Label_Amount;
    private javax.swing.JLabel tkOrd_Label_AmountVal;
    private javax.swing.JLabel tkOrd_Label_Client;
    private javax.swing.JLabel tkOrd_Label_Deliv;
    private javax.swing.JLabel tkOrd_Label_Dest;
    private javax.swing.JLabel tkOrd_Label_DiscM;
    private javax.swing.JLabel tkOrd_Label_Discount;
    private javax.swing.JLabel tkOrd_Label_DiscountVal;
    private javax.swing.JLabel tkOrd_Label_Prod;
    private javax.swing.JLabel tkOrd_Label_Qty;
    private javax.swing.JLabel tkOrd_Label_Tax;
    private javax.swing.JLabel tkOrd_Label_TaxVal;
    private javax.swing.JLabel tkOrd_Label_Total;
    private javax.swing.JLabel tkOrd_Label_TotalVal;
    private javax.swing.JComboBox<String> tkOrd_List_Client;
    private javax.swing.JComboBox<String> tkOrd_List_Prod;
    private javax.swing.JLabel tkOrd_ScreenNameLabel;
    private javax.swing.JLabel tkOrd_TimeLabel2;
    private javax.swing.JLabel tkOrd_UserNameLabel;
    // End of variables declaration//GEN-END:variables
}
