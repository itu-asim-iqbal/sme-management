/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database_finalproject;
//import com.toedter.calendar.JTextFieldDateEditor;
import java.awt.Toolkit;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
/**
 *
 * @author Murtaza
 */
public class Murtaza_edit_order_screen extends javax.swing.JFrame{

    /**
     * Creates new form Murtaza_edit_order_screen
     */
    private void update_prices()
    {
        int prod_index=product_id_select.getSelectedIndex();
        String[] s=(String[])prod_class.values.get(prod_index);
        Integer prod_cost=Integer.parseInt(s[2]);
        Integer qty=Integer.parseInt(order_quantity_text.getText());
        prod_cost=prod_cost.intValue()*qty.intValue();
        if(!discount_text.getText().equals(""))
        {
            Integer discount=Integer.parseInt(discount_text.getText());
            prod_cost=prod_cost.intValue()-((prod_cost.intValue()/100)*discount.intValue());
        }
        order_price_text.setText(prod_cost.toString());

        Double a=Double.parseDouble(order_price_text.getText());
        a=a.doubleValue()*0.15;
        Integer b=a.intValue();
        order_tax_text.setText(b.toString());

        Integer x=Integer.parseInt(order_price_text.getText());
        Integer y=Integer.parseInt(order_tax_text.getText());
        Integer z=x+y;
        total_amount_text.setText(z.toString());
    }
    private Murtaza_populateProducts prod_class;
    public boolean product_populate_flag;
    
    
    private void populateFields()
    {
        try
        {
//            Class.forName("oracle.jdbc.driver.OracleDriver");
//            Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl",Asim_Database_FinalProject.qtm.USER,Asim_Database_FinalProject.qtm.PASS);
            Asim_Database_FinalProject.qtm.initDB( Asim_Database_FinalProject.qtm.DB_URL ) ;
            Connection con = Asim_Database_FinalProject.qtm.db ;
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery("select * from AK_ORDER WHERE ORDER_ID="+order_id);
            rs.next();
            
            dest_text.setText(rs.getString(7));
            order_price_text.setText(rs.getString(9));
            order_tax_text.setText(rs.getString(8));
            
            order_quantity_text.setText(rs.getString(4));
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString(6));
            date_select.setDate(d);
            if(rs.getString(10).toLowerCase().equals("approval pending"))
            {
                order_status_select.setSelectedIndex(0);
            }
            else if(rs.getString(10).toLowerCase().equals("approved"))
            {
                order_status_select.setSelectedIndex(1);
            }
            else if(rs.getString(10).toLowerCase().equals("production"))
            {
                order_status_select.setSelectedIndex(2);
            }
            else if(rs.getString(10).toLowerCase().equals("completed"))
            {
                order_status_select.setSelectedIndex(3);
            }
            else if(rs.getString(10).toLowerCase().equals("delivered"))
            {
                order_status_select.setSelectedIndex(4);
            }
            else
            {
                order_status_select.setSelectedIndex(0);
            }
            prod_class=new Murtaza_populateProducts(product_id_select, rs.getString(2), this);
            String[] s=(String[])prod_class.values.get(product_id_select.getSelectedIndex());
            Integer prod_cost=Integer.parseInt(s[2]);
            Integer qty_integer=Integer.parseInt(order_quantity_text.getText());
            int orignal_cost=qty_integer.intValue() * prod_cost.intValue();
            
            Integer order_price=Integer.parseInt(rs.getString(9));
            Integer order_tax=Integer.parseInt(rs.getString(8));
            order_price=order_price.intValue()-order_tax.intValue();
            //order_price=order_price.intValue()/orignal_cost;
            order_price=(orignal_cost-order_price.intValue())*100;
            order_price=order_price.intValue()/orignal_cost;
            discount_text.setText(order_price.toString());
            
            update_prices();
            con.close();
            
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
    }
    
    private String order_id;
    public Murtaza_edit_order_screen(String oid)
    {
        product_populate_flag=false;
        order_id=oid;
        this.setUndecorated(true);
//        this.setAlwaysOnTop(true);
        this.setResizable(false);
        this.setVisible(true);
        initComponents();

        Toolkit tk=Toolkit.getDefaultToolkit();
        int xsize=(int)tk.getScreenSize().getWidth();
        int ysize=(int)tk.getScreenSize().getHeight();
        this.setSize(xsize, ysize);
        
        //this.setUndecorated(true);
        populateFields();
        order_quantity_text.getDocument().addDocumentListener(new DocumentListener()
        {

            public void changedUpdate(DocumentEvent e){}
            public void removeUpdate(DocumentEvent e)
            {
                if(order_quantity_text.getText().equals(""))
                {
                    order_price_text.setText("0");
                    order_tax_text.setText("0");
                    total_amount_text.setText("0");
                }
                else
                {
                    update_prices();
                }
            }
            public void insertUpdate(DocumentEvent e)
            {
                if(order_quantity_text.getText().equals(""))
                {
                    order_price_text.setText("0");
                    order_tax_text.setText("0");
                    total_amount_text.setText("0");
                }
                else
                {
                    update_prices();
                }
            }
        });
        
        
        discount_text.getDocument().addDocumentListener(new DocumentListener()
        {

            public void changedUpdate(DocumentEvent e){}
            public void removeUpdate(DocumentEvent e)
            {
                if(order_quantity_text.getText().equals(""))
                {
                    order_price_text.setText("0");
                    order_tax_text.setText("0");
                    total_amount_text.setText("0");
                }
                else
                {
                    update_prices();
                }
            }
            public void insertUpdate(DocumentEvent e)
            {
                if(order_quantity_text.getText().equals(""))
                {
                    order_price_text.setText("0");
                    order_tax_text.setText("0");
                    total_amount_text.setText("0");
                }
                else
                {
                    update_prices();
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgPanel = new javax.swing.JPanel();
        prod_id_label = new javax.swing.JLabel();
        del_date_label = new javax.swing.JLabel();
        dest_label = new javax.swing.JLabel();
        quantity_label = new javax.swing.JLabel();
        status_label = new javax.swing.JLabel();
        product_id_select = new javax.swing.JComboBox();
        date_select = new com.toedter.calendar.JDateChooser();
        dest_text = new javax.swing.JTextField();
        order_quantity_text = new javax.swing.JTextField();
        order_status_select = new javax.swing.JComboBox();
        save_button = new javax.swing.JButton();
        cancel_button = new javax.swing.JButton();
        delete_button = new javax.swing.JButton();
        order_price_label = new javax.swing.JLabel();
        order_tax_label = new javax.swing.JLabel();
        total_amount_label = new javax.swing.JLabel();
        order_price_text = new javax.swing.JLabel();
        order_tax_text = new javax.swing.JLabel();
        total_amount_text = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        discount_label = new javax.swing.JLabel();
        discount_text = new javax.swing.JTextField();
        edit_order_label = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        bgPanel = new MyCanvas ( ) ;

        prod_id_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        prod_id_label.setText("Product ID:");

        del_date_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        del_date_label.setText("Delivery Date:");

        dest_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        dest_label.setText("Destination:");

        quantity_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        quantity_label.setText("Order Quantity:");

        status_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        status_label.setText("Order Status:");

        product_id_select.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        product_id_select.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                product_id_selectActionPerformed(evt);
            }
        });

        date_select.setDateFormatString("dd-MM-yyyy");
        date_select.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        dest_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        order_quantity_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        order_status_select.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        order_status_select.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Approval Pending", "Approved", "Under Process", "Completed", "Delivered" }));

        save_button.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        save_button.setText("Save");
        save_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                save_buttonActionPerformed(evt);
            }
        });

        cancel_button.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        cancel_button.setText("Cancel");
        cancel_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancel_buttonActionPerformed(evt);
            }
        });

        delete_button.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        delete_button.setText("Delete");
        delete_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delete_buttonActionPerformed(evt);
            }
        });

        order_price_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        order_price_label.setText("Order Price:");

        order_tax_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        order_tax_label.setText("Order Tax:");

        total_amount_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        total_amount_label.setText("Total Amount:");

        order_price_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        order_price_text.setText("20000");

        order_tax_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        order_tax_text.setText("100000");

        total_amount_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        total_amount_text.setText("23000");

        discount_label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        discount_label.setText("Discount:");

        discount_text.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        edit_order_label.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        edit_order_label.setText("Edit Order");

        javax.swing.GroupLayout bgPanelLayout = new javax.swing.GroupLayout(bgPanel);
        bgPanel.setLayout(bgPanelLayout);
        bgPanelLayout.setHorizontalGroup(
            bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bgPanelLayout.createSequentialGroup()
                .addGap(299, 299, 299)
                .addComponent(edit_order_label)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(bgPanelLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bgPanelLayout.createSequentialGroup()
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(bgPanelLayout.createSequentialGroup()
                                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(del_date_label)
                                    .addComponent(dest_label))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(dest_text)
                                    .addComponent(date_select, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(bgPanelLayout.createSequentialGroup()
                                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(prod_id_label)
                                    .addComponent(quantity_label))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(product_id_select, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(order_quantity_text, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(320, 320, 320))
                    .addGroup(bgPanelLayout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(bgPanelLayout.createSequentialGroup()
                                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(order_price_label)
                                    .addComponent(order_tax_label)
                                    .addComponent(total_amount_label)
                                    .addComponent(status_label)
                                    .addComponent(discount_label))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(order_tax_text, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(total_amount_text, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(order_status_select, javax.swing.GroupLayout.Alignment.TRAILING, 0, 344, Short.MAX_VALUE)
                                    .addComponent(discount_text)
                                    .addComponent(order_price_text, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(320, 320, 320))
                            .addGroup(bgPanelLayout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 587, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 587, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(bgPanelLayout.createSequentialGroup()
                                .addComponent(save_button, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(cancel_button, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(delete_button, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))))))
        );
        bgPanelLayout.setVerticalGroup(
            bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bgPanelLayout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(edit_order_label)
                .addGap(18, 18, 18)
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(prod_id_label)
                    .addComponent(product_id_select, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(order_quantity_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(quantity_label))
                .addGap(18, 18, 18)
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(del_date_label)
                    .addComponent(date_select, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dest_label)
                    .addComponent(dest_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(order_status_select, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(status_label))
                .addGap(18, 18, 18)
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(discount_label)
                    .addComponent(discount_text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(order_price_label)
                    .addComponent(order_price_text))
                .addGap(18, 18, 18)
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(order_tax_label)
                    .addComponent(order_tax_text))
                .addGap(18, 18, 18)
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(total_amount_label)
                    .addComponent(total_amount_text))
                .addGap(18, 18, 18)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(save_button, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cancel_button, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(delete_button, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(89, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 804, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(bgPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, 0)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 673, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(bgPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, 0)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void cancel_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancel_buttonActionPerformed
        // TODO add your handling code here:
        //edit_order.setVisible(true);
        if (JOptionPane.showConfirmDialog(null, "Are you sure you want to CANCEL this operation?", "Confirm",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
        {
             if (JOptionPane.showConfirmDialog(null, "Are you sure you want to return to the previos screen?", "Confirm",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
            {
                Murtaza_EditOrder ed=new Murtaza_EditOrder();
                ed.setVisible(true);
                this.dispose();
            }
            else
            {
                return ;
            }
        }
        else
        {
            return ;
        }
    }//GEN-LAST:event_cancel_buttonActionPerformed

    private void product_id_selectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_product_id_selectActionPerformed
        if(product_populate_flag==true)
        {
            update_prices();
        }
    }//GEN-LAST:event_product_id_selectActionPerformed

    private void save_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_save_buttonActionPerformed
//        System.out.println(product_id_select.getSelectedItem());
//        System.out.println(order_quantity_text.getText());
////        Date d = new SimpleDateFormat("yyyy-MM-dd").parse(date_select.getDate());
////        System.out.println(d.toString);
//      Date d=date_select.getDate();Ww
     if (JOptionPane.showConfirmDialog(null, "Are you sure you want to SAVE these changes?", "Confirm",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
    {    
        try
            {
//                Class.forName("oracle.jdbc.driver.OracleDriver");
//                Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl",Asim_Database_FinalProject.qtm.USER,Asim_Database_FinalProject.qtm.PASS);
                 Asim_Database_FinalProject.qtm.initDB( Asim_Database_FinalProject.qtm.DB_URL ) ;
                Connection con = Asim_Database_FinalProject.qtm.db ;
                Statement stmt=con.createStatement();
                int prod_index=product_id_select.getSelectedIndex();
                String[] s=(String[])prod_class.values.get(prod_index);
                Integer prod_ID=Integer.parseInt(s[0]);
                //Date d=date_select.getDate();
                //Date d = new SimpleDateFormat("dd-MM-yyyy").parse(date_select.getDate().toString());
                DateFormat df= new SimpleDateFormat("dd-MM-yyyy");
                String d=df.format(date_select.getDate());
                stmt.executeQuery("UPDATE AK_ORDER SET PROD_ID="+prod_ID.toString()+" WHERE ORDER_ID="+order_id);
                System.out.println("Prod ID.");
                System.out.println("UPDATE AK_ORDER SET DELIV_DATE=TO_DATE('"+d+"','DD-MM-YYYY') WHERE ORDER_ID="+order_id);
                stmt.executeQuery("UPDATE AK_ORDER SET DELIV_DATE=TO_DATE(' "+d+" ','DD-MM-YYYY') WHERE ORDER_ID="+order_id);
                stmt.executeQuery("UPDATE AK_ORDER SET ORDER_QTY="+order_quantity_text.getText()+" WHERE ORDER_ID="+order_id);
                stmt.executeQuery("UPDATE AK_ORDER SET ORDER_DEST='"+dest_text.getText()+"' WHERE ORDER_ID="+order_id);
                stmt.executeQuery("UPDATE AK_ORDER SET ORDER_STATUS='"+(String)order_status_select.getSelectedItem()+"' WHERE ORDER_ID="+order_id);
                stmt.executeQuery("UPDATE AK_ORDER SET ORDER_PRICE="+total_amount_text.getText()+" WHERE ORDER_ID="+order_id);
                stmt.executeQuery("UPDATE AK_ORDER SET ORDER_TAX="+order_tax_text.getText()+" WHERE ORDER_ID="+order_id);
                con.close();
                Murtaza_EditOrder ed=new Murtaza_EditOrder();
                ed.setVisible(true);
                this.dispose();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
    }
     else
     {
         return ;
     }
        
        
    }//GEN-LAST:event_save_buttonActionPerformed

    private void delete_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delete_buttonActionPerformed
        // TODO add your handling code here:
        if (JOptionPane.showConfirmDialog(null, "Are you sure you want to DELETE  this entry?", "Confirm",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
        {
            try
            {

//                Class.forName("oracle.jdbc.driver.OracleDriver");
//                Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl",Asim_Database_FinalProject.qtm.USER,Asim_Database_FinalProject.qtm.PASS);
                 Asim_Database_FinalProject.qtm.initDB( Asim_Database_FinalProject.qtm.DB_URL ) ;
                Connection con = Asim_Database_FinalProject.qtm.db ;
                Statement stmt=con.createStatement();
                stmt.executeQuery("DELETE FROM AK_ORDER WHERE order_id="+order_id);
                con.close();
                Murtaza_EditOrder ed=new Murtaza_EditOrder();
                ed.setVisible(true);
                this.dispose();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            return ;
        }
    }//GEN-LAST:event_delete_buttonActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(Murtaza_edit_order_screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(Murtaza_edit_order_screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(Murtaza_edit_order_screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(Murtaza_edit_order_screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new Murtaza_edit_order_screen().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bgPanel;
    private javax.swing.JButton cancel_button;
    private com.toedter.calendar.JDateChooser date_select;
    private javax.swing.JLabel del_date_label;
    private javax.swing.JButton delete_button;
    private javax.swing.JLabel dest_label;
    private javax.swing.JTextField dest_text;
    private javax.swing.JLabel discount_label;
    private javax.swing.JTextField discount_text;
    private javax.swing.JLabel edit_order_label;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel order_price_label;
    private javax.swing.JLabel order_price_text;
    private javax.swing.JTextField order_quantity_text;
    private javax.swing.JComboBox order_status_select;
    private javax.swing.JLabel order_tax_label;
    private javax.swing.JLabel order_tax_text;
    private javax.swing.JLabel prod_id_label;
    private javax.swing.JComboBox product_id_select;
    private javax.swing.JLabel quantity_label;
    private javax.swing.JButton save_button;
    private javax.swing.JLabel status_label;
    private javax.swing.JLabel total_amount_label;
    private javax.swing.JLabel total_amount_text;
    // End of variables declaration//GEN-END:variables
}
