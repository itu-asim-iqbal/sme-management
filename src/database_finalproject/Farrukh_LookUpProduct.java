package database_finalproject;
import java.awt.Toolkit;
import javax.swing.JTable;
import javax.swing.ButtonGroup;
import javax.swing.table.DefaultTableModel;
import javax.swing.event.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.table.*;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author farrukh
 */
public class Farrukh_LookUpProduct extends javax.swing.JFrame  {
      public SqlQueriesFunction queryObj;
      public ButtonGroup bg1;
      public int row=10000;
      public int col;
      public boolean SearchCheck;
      public String SearchSql; 
      public  String User=Asim_Database_FinalProject.qtm.USER;
      public  String Password=Asim_Database_FinalProject.qtm.PASS;
      Asim_TimerThreadClass timerThread;
      //public ButtonModel;
    private  DefaultTableModel tableModel ;
    /**
     * Creates new formookUpProduct
     */
    
    private void groupButton(){
        bg1=new ButtonGroup();
        bg1.add(PROD_ID);
        bg1.add(PROD_STYLE);
        bg1.add(PROD_TYPE);
        bg1.add(PROD_NAME);
        bg1.add(PROD_SIZE);
        bg1.add(PROD_THREAD);
        bg1.add(PROD_BUTTON);
        bg1.add(PROD_ZIP);
        bg1.add(PROD_PRINT);
        bg1.add(PROD_TIME);
    
    }
    
    
    public Farrukh_LookUpProduct() {
        
       tableModel = new DefaultTableModel() {

    @Override
    public boolean isCellEditable(int row, int column) {
       //all cells false
       return false;
    }
};
        this.setUndecorated(true);
//        this.setAlwaysOnTop(true);
        this.setResizable(false);
        this.setVisible(true);
                       
        
        initComponents();
        Toolkit tk = Toolkit.getDefaultToolkit();
        int xsize = (int)tk.getScreenSize().getWidth();
        int ysize = (int)tk.getScreenSize().getHeight();
        this.setSize(xsize , ysize);
        
        timerThread = new Asim_TimerThreadClass(DateLabel , TimeLabel);
        timerThread.start();
        
        PROD_ID.setSelected(true);// By Default
        SaveButton.setVisible(false);
       // System.out.println("DELETE FROM ak_products WHERE PROD_ID = "+ Product_Table.getModel().getValueAt(0,0));
        SearchTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
              //  System.out.println("Insert");
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                //System.out.println("Removed");
                if(SearchTextField.getText().trim().length() == 0){
                    System.out.println("Deploy");
                    SqlQueriesFunction.setTable(User, Password, "Products",Product_Table, tableModel);
                }
                else{
                    
                }
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                //System.out.println("Changed");
            }
        });
        groupButton();  
        SqlQueriesFunction.setTable(User, Password, "Products",Product_Table, tableModel);
        this.setExtendedState(this.MAXIMIZED_BOTH);
        
        
        Product_Table.addMouseListener( new MouseAdapter(){     
            public void mousePressed( MouseEvent e )
            {       
                    // Left mouse click
                    if ( SwingUtilities.isLeftMouseButton( e ) )
                    {   //Product_Table.getModel().getValueAt(row_index, col_index);
                         
                       System.out.println("MouseUsed");
                            // Do something
                             // get the coordinates of the mouse click
                            Point p = e.getPoint();
                            
                            // get the row index that contains that coordinate
                            int rowNumber = Product_Table.rowAtPoint( p );
                            int colNumber=Product_Table.columnAtPoint(p);
                            //System.out.println("Row Number "+rowNumber);
                            // Get the ListSelectionModel of the JTable
                            ListSelectionModel model = Product_Table.getSelectionModel();

                            // set the selected interval of rows. Using the "rowNumber"
                            // variable for the beginning and end selects only that one row.
                            //model.setSelectionInterval( 1, rowNumber );
                            Product_Table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                            Product_Table.setColumnSelectionAllowed(false);
                            Product_Table.setRowSelectionAllowed(true);

                             row = rowNumber;
                             col = colNumber;
                            boolean toggle = true;
                            boolean extend = true;
                            
                               
                           // Product_Table.changeSelection(rowNumber,colNumber, toggle, extend);
                            
                            //Product_Table.setRowSelectionInterval(rowNumber, colNumber);
                    }
                    // Right mouse click
                   
            }
                         });
        
        
        if ( Asim_users_login.accessLevel.equals ( "1" ) || Asim_users_login.accessLevel.equals ( "2" ) )
        {
            DeleteButton.setVisible( false ) ;
            EditButton.setVisible( false ) ;
            CurrentValueLable.setVisible(false);
            ColNameLabel.setVisible(false);
            ColValue.setVisible(false);
            NewValueLabel.setVisible(false);
            ColumnNameLabel.setVisible(false);
            ValueOptions.setVisible(false);
            SaveButton.setVisible(false);
        }
        else if ( ! AK_MS_Lvl3.farrukhToBlame )
        {
            DeleteButton.setVisible( false ) ;
            EditButton.setVisible( false ) ;
            CurrentValueLable.setVisible(false);
            ColNameLabel.setVisible(false);
            ColValue.setVisible(false);
            NewValueLabel.setVisible(false);
            ColumnNameLabel.setVisible(false);
            ValueOptions.setVisible(false);
            SaveButton.setVisible(false);
        }
            
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        bgPanel = new javax.swing.JPanel();
        UpperPanel = new javax.swing.JPanel();
        Home = new javax.swing.JButton();
        MainScreenText = new javax.swing.JLabel();
        DateLabel = new javax.swing.JLabel();
        TimeLabel = new javax.swing.JLabel();
        MiddlePanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        Product_Table = new javax.swing.JTable();
        LowerPanel = new javax.swing.JPanel();
        SearchTextField = new javax.swing.JTextField();
        SearchButton = new javax.swing.JButton();
        PROD_ID = new javax.swing.JRadioButton();
        PROD_TYPE = new javax.swing.JRadioButton();
        PROD_NAME = new javax.swing.JRadioButton();
        PROD_STYLE = new javax.swing.JRadioButton();
        PROD_SIZE = new javax.swing.JRadioButton();
        PROD_THREAD = new javax.swing.JRadioButton();
        PROD_BUTTON = new javax.swing.JRadioButton();
        PROD_ZIP = new javax.swing.JRadioButton();
        PROD_PRINT = new javax.swing.JRadioButton();
        PROD_TIME = new javax.swing.JRadioButton();
        DeleteButton = new javax.swing.JButton();
        EditButton = new javax.swing.JButton();
        ColValue = new javax.swing.JLabel();
        CurrentValueLable = new javax.swing.JLabel();
        ColNameLabel = new javax.swing.JLabel();
        NewValueLabel = new javax.swing.JLabel();
        ColumnNameLabel = new javax.swing.JLabel();
        ValueOptions = new javax.swing.JComboBox<>();
        SaveButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        bgPanel = new MyCanvas ( ) ;

        UpperPanel.setOpaque(false);

        Home.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Home.setText("Home");
        Home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HomeActionPerformed(evt);
            }
        });

        MainScreenText.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        MainScreenText.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        MainScreenText.setText("Lookup Products");
        MainScreenText.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        DateLabel.setText("Date");

        TimeLabel.setText("Time");

        javax.swing.GroupLayout UpperPanelLayout = new javax.swing.GroupLayout(UpperPanel);
        UpperPanel.setLayout(UpperPanelLayout);
        UpperPanelLayout.setHorizontalGroup(
            UpperPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(UpperPanelLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(Home, javax.swing.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)
                .addGap(201, 201, 201)
                .addComponent(MainScreenText, javax.swing.GroupLayout.DEFAULT_SIZE, 440, Short.MAX_VALUE)
                .addGap(291, 291, 291)
                .addGroup(UpperPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(DateLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(UpperPanelLayout.createSequentialGroup()
                        .addComponent(TimeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(1, 1, 1)))
                .addGap(67, 67, 67))
        );
        UpperPanelLayout.setVerticalGroup(
            UpperPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(UpperPanelLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(UpperPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(UpperPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(MainScreenText)
                        .addComponent(TimeLabel))
                    .addComponent(DateLabel)
                    .addComponent(Home, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        MiddlePanel.setOpaque(false);

        Product_Table.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        //Product_Table.setModel(AwabKnitWear.ul.qtm);
        Product_Table.setModel(tableModel);
        Product_Table.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane1.setViewportView(Product_Table);
        //Product_Table.setViewportView(Product_Table);
        Product_Table.setCellSelectionEnabled(true);
        System.out.println("Checking ");
        jScrollPane1.setViewportView(Product_Table);
        Product_Table.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        for ( int i = 0 ; i < Product_Table.getColumnCount();i++ )
        {
            Product_Table.getColumnModel().getColumn(i).setMinWidth(100);
        }
        //System.out.println("Here I am"+Product_Table.getColumnCount());

        javax.swing.GroupLayout MiddlePanelLayout = new javax.swing.GroupLayout(MiddlePanel);
        MiddlePanel.setLayout(MiddlePanelLayout);
        MiddlePanelLayout.setHorizontalGroup(
            MiddlePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, MiddlePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        MiddlePanelLayout.setVerticalGroup(
            MiddlePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 343, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        LowerPanel.setOpaque(false);

        SearchTextField.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        SearchTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SearchTextFieldActionPerformed(evt);
            }
        });

        SearchButton.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        SearchButton.setText("Search");
        SearchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SearchButtonActionPerformed(evt);
            }
        });

        PROD_ID.setText("Product ID");
        PROD_ID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PROD_IDActionPerformed(evt);
            }
        });

        PROD_TYPE.setText("Product Type");
        PROD_TYPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PROD_TYPEActionPerformed(evt);
            }
        });

        PROD_NAME.setText("Product Name");

        PROD_STYLE.setText("Product Style");

        PROD_SIZE.setText("Product Size");
        PROD_SIZE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PROD_SIZEActionPerformed(evt);
            }
        });

        PROD_THREAD.setText("Product Thread");

        PROD_BUTTON.setText("Product Button");

        PROD_ZIP.setText("Product Zip");

        PROD_PRINT.setText("Product Print");

        PROD_TIME.setText("Product Time");
        PROD_TIME.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PROD_TIMEActionPerformed(evt);
            }
        });

        DeleteButton.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        DeleteButton.setText("Delete");
        DeleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DeleteButtonActionPerformed(evt);
            }
        });

        EditButton.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        EditButton.setText("Edit");
        EditButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditButtonActionPerformed(evt);
            }
        });

        ColValue.setText("Value");

        CurrentValueLable.setText("Current");

        ColNameLabel.setText("ColumnName");

        NewValueLabel.setText("New");

        ColumnNameLabel.setText("ColumnName");

        SaveButton.setText("Save");
        SaveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout LowerPanelLayout = new javax.swing.GroupLayout(LowerPanel);
        LowerPanel.setLayout(LowerPanelLayout);
        LowerPanelLayout.setHorizontalGroup(
            LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LowerPanelLayout.createSequentialGroup()
                .addGroup(LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(LowerPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(LowerPanelLayout.createSequentialGroup()
                                .addGroup(LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(LowerPanelLayout.createSequentialGroup()
                                        .addComponent(PROD_ID, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(20, 20, 20))
                                    .addComponent(PROD_BUTTON, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(34, 34, 34))
                            .addGroup(LowerPanelLayout.createSequentialGroup()
                                .addComponent(PROD_STYLE, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                                .addGap(46, 46, 46)))
                        .addGroup(LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(PROD_NAME, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                            .addGroup(LowerPanelLayout.createSequentialGroup()
                                .addComponent(PROD_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(8, 8, 8))
                            .addGroup(LowerPanelLayout.createSequentialGroup()
                                .addComponent(PROD_ZIP, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(14, 14, 14))
                            .addComponent(PROD_TIME, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(40, 40, 40)
                        .addGroup(LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(LowerPanelLayout.createSequentialGroup()
                                .addComponent(PROD_PRINT, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE)
                                .addGap(12, 12, 12))
                            .addComponent(PROD_THREAD, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(LowerPanelLayout.createSequentialGroup()
                                .addComponent(PROD_TYPE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(10, 10, 10)))
                        .addGap(411, 411, 411))
                    .addGroup(LowerPanelLayout.createSequentialGroup()
                        .addComponent(SearchTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(SearchButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(73, 73, 73)
                        .addComponent(DeleteButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(82, 82, 82)))
                .addGroup(LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(EditButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(LowerPanelLayout.createSequentialGroup()
                        .addGroup(LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(LowerPanelLayout.createSequentialGroup()
                                .addComponent(CurrentValueLable, javax.swing.GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
                                .addGap(2, 2, 2))
                            .addGroup(LowerPanelLayout.createSequentialGroup()
                                .addComponent(NewValueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(24, 24, 24)))
                        .addGroup(LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ColumnNameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
                            .addComponent(ColNameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(LowerPanelLayout.createSequentialGroup()
                                .addComponent(SaveButton, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                                .addGap(5, 5, 5))
                            .addComponent(ColValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(LowerPanelLayout.createSequentialGroup()
                                .addComponent(ValueOptions, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(11, 11, 11)))))
                .addGap(92, 92, 92))
        );
        LowerPanelLayout.setVerticalGroup(
            LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, LowerPanelLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(DeleteButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(SearchTextField)
                    .addComponent(SearchButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE)
                    .addComponent(EditButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addGroup(LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PROD_ID)
                    .addComponent(PROD_NAME)
                    .addComponent(PROD_TYPE)
                    .addComponent(ColValue, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CurrentValueLable, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ColNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PROD_THREAD)
                    .addGroup(LowerPanelLayout.createSequentialGroup()
                        .addGroup(LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(NewValueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ColumnNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ValueOptions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(SaveButton))
                    .addGroup(LowerPanelLayout.createSequentialGroup()
                        .addGroup(LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(PROD_SIZE)
                            .addGroup(LowerPanelLayout.createSequentialGroup()
                                .addComponent(PROD_STYLE)
                                .addGap(18, 18, 18)
                                .addGroup(LowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(PROD_BUTTON)
                                    .addComponent(PROD_ZIP)
                                    .addComponent(PROD_PRINT))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(PROD_TIME, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(78, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout bgPanelLayout = new javax.swing.GroupLayout(bgPanel);
        bgPanel.setLayout(bgPanelLayout);
        bgPanelLayout.setHorizontalGroup(
            bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(UpperPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(MiddlePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bgPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LowerPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        bgPanelLayout.setVerticalGroup(
            bgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bgPanelLayout.createSequentialGroup()
                .addComponent(UpperPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(MiddlePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(LowerPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(4, 4, 4))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1213, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(bgPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, 0)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 775, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(bgPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, 0)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SearchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SearchButtonActionPerformed
       String sql;
       String RadioButton="";
       sql=SearchTextField.getText();
       //Product_Table.changeSelection(1, 1, false, false);
       
        if(PROD_ID.isSelected()){RadioButton="PROD_ID" ; }
        if(PROD_NAME.isSelected()){RadioButton="PROD_NAME";}
        if(PROD_TYPE.isSelected()){RadioButton="PROD_TYPE";}
        if(PROD_STYLE.isSelected()){RadioButton="PROD_STYLE";}
        if(PROD_SIZE.isSelected()){RadioButton="PROD_SIZE";}
        if(PROD_THREAD.isSelected()){RadioButton="PROD_THREAD";}
        if(PROD_ZIP.isSelected()){RadioButton="PROD_ZIP";}
        if(PROD_BUTTON.isSelected()){RadioButton="PROD_BUTTON";}
        if(PROD_PRINT.isSelected()){RadioButton="PROD_PRINT";}
        if(PROD_TIME.isSelected()){RadioButton="PROD_TIME_MINS";}
        //System.out.println("Search field "+sql);
        //System.out.println("RADIOSELexted "+RadioButton);
        sql="Select * from ak_products WHERE upper(" + RadioButton +") LIKE upper('%"+sql+"%')";
        SearchSql=sql ;
        SqlQueriesFunction.setTable(User, Password, "Products",Product_Table, tableModel,sql);// TODO add your handling code here:
        SearchCheck=true;
        
        
    }//GEN-LAST:event_SearchButtonActionPerformed

    private void PROD_IDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PROD_IDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PROD_IDActionPerformed

    private void PROD_TYPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PROD_TYPEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PROD_TYPEActionPerformed

    private void SearchTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SearchTextFieldActionPerformed
                // TODO add your handling code here:
               
                //SearchTextField.getDocument().addDocumentListener();
                 //  SearchTextField.getDocument().addDocumentListener(listener);
                
                    
    }//GEN-LAST:event_SearchTextFieldActionPerformed

    private void DeleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DeleteButtonActionPerformed
        
        
        String sql="DELETE FROM ak_products WHERE PROD_ID = "+ Product_Table.getModel().getValueAt(row,0);  // TODO add your handling code here:
        System.out.println("DELETE FROM ak_products WHERE PROD_ID = "+ Product_Table.getModel().getValueAt(row,0));
        SqlQueriesFunction.setTableDelete(User, Password, "Products",Product_Table, tableModel,sql);
       
        //if User has deleted through search
        if(SearchCheck==true){
        
        SqlQueriesFunction.setTable(User, Password, "Products",Product_Table, tableModel,SearchSql);
        SearchCheck=false;
        }
        else{
         SqlQueriesFunction.setTable(User, Password, "Products",Product_Table, tableModel);
        }
    }//GEN-LAST:event_DeleteButtonActionPerformed
    
  
    private void EditButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EditButtonActionPerformed
       //JOptionPane.showMessageDialog(null, "Welcome");
        //System.out.println("Edit FROM ak_products WHERE PROD_ID  = "+ Product_Table.getModel().getValueAt(row,0));
        ColNameLabel.setText(Product_Table.getModel().getColumnName(col).toString());
        ColumnNameLabel.setText(Product_Table.getModel().getColumnName(col).toString());
        ColValue.setText(Product_Table.getModel().getValueAt(row,col).toString());
        
        if(Product_Table.getModel().getColumnName(col).toString().equals("PROD_ID")){
           
        String [] PROD_ID_DATA={""};
        ValueOptions.setModel(new javax.swing.DefaultComboBoxModel(PROD_ID_DATA)); 
        
        }
        
        
       if(Product_Table.getModel().getColumnName(col).toString().equals("PROD_NAME")){
           
        String [] PROD_NAME_DATA={"Jeans","Coats","Dresses","T-shirts","Jackets","Pants","Trousers","Sweat Shirts","Cardigans","Suits","Socks","Nighties","Ties","Vests","Caps","Hoodies"};
        ValueOptions.setModel(new javax.swing.DefaultComboBoxModel(PROD_NAME_DATA)); 
        
        }
        
        
        if(Product_Table.getModel().getColumnName(col).toString().equals("PROD_TYPE")){
        
            String [] PROD_TYPE_DATA={"A","B","C","D"};
            ValueOptions.setModel(new javax.swing.DefaultComboBoxModel(PROD_TYPE_DATA));
        
        }
        if(Product_Table.getModel().getColumnName(col).toString().equals("PROD_STYLE")){
        
            String [] PROD_STYLE_DATA={"Classic","Modern","Bohemian","Arty","Flamboyant","Glamorous","Sphisticated","Traditional","Preppy","Tomboy","Goth","Modern","Western","Punk","Geeky"};
            ValueOptions.setModel(new javax.swing.DefaultComboBoxModel(PROD_STYLE_DATA));
        
        }
        if(Product_Table.getModel().getColumnName(col).toString().equals("PROD_SIZE")){
        
            String [] PROD_SIZE_DATA={"XS","S","M","L","Xl"};
        
            ValueOptions.setModel(new javax.swing.DefaultComboBoxModel(PROD_SIZE_DATA));
        
        }
        if(Product_Table.getModel().getColumnName(col).toString().equals("PROD_THREAD")){
        
            String [] PROD_THREAD_DATA={};
            ValueOptions.setModel(new javax.swing.DefaultComboBoxModel(PROD_THREAD_DATA));
        
        }
        if(Product_Table.getModel().getColumnName(col).toString().equals("PROD_BUTTON")){
        
            String [] PROD_BUTTON_DATA={};
            ValueOptions.setModel(new javax.swing.DefaultComboBoxModel(PROD_BUTTON_DATA));
        
        }
        if(Product_Table.getModel().getColumnName(col).toString().equals("PROD_ZIP")){
        
            String [] PROD_ZIP_DATA={};
            ValueOptions.setModel(new javax.swing.DefaultComboBoxModel(PROD_ZIP_DATA));
        
        }if(Product_Table.getModel().getColumnName(col).toString().equals("PROD_PRINT")){
        
            String [] PROD_PRINT_DATA={"A","B","C","D"};
            ValueOptions.setModel(new javax.swing.DefaultComboBoxModel(PROD_PRINT_DATA));
        
        }
        if(Product_Table.getModel().getColumnName(col).toString().equals("PROD_TIME_MINS")){
        
       /* String [] PROD_TIME_MINS_DATA={};
        ValueOptions.setModel(new javax.swing.DefaultComboBoxModel(PROD_TIME_MINS_DATA));*/
       for(int i=0;i<5000;i++){
           ValueOptions.addItem( Integer.toString(i));
       }
        
        }
        

        SaveButton.setVisible(true);
    // TODO add your handling code here:
    }//GEN-LAST:event_EditButtonActionPerformed

    private void PROD_SIZEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PROD_SIZEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PROD_SIZEActionPerformed

    private void PROD_TIMEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PROD_TIMEActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PROD_TIMEActionPerformed

    private void SaveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveButtonActionPerformed
        String ProdID=Product_Table.getModel().getValueAt(row,0).toString();
        String Column_Name=Product_Table.getModel().getColumnName(col).toString();
        String SelectedItem="'"+ValueOptions.getSelectedItem()+"'";
        
        String sql="UPDATE ak_products"+" SET "+Column_Name+"="+SelectedItem+" WHERE PROD_ID"+"="+ProdID;
        
        // TODO add your handling code here:
         // TODO add your handling code here:
       // System.out.println("DELETE FROM ak_products WHERE PROD_ID  = "+ Product_Table.getModel().getColumnName(col));
        
       SqlQueriesFunction.setTable(User, Password, "Products",Product_Table, tableModel,sql);
        //if User has edited through search
        if(SearchCheck==true){
        
        SqlQueriesFunction.setTable(User, Password, "Products",Product_Table, tableModel,SearchSql);
        SearchCheck=false;
        }
        else{
         SqlQueriesFunction.setTable(User, Password, "Products",Product_Table, tableModel);
        }
         SaveButton.setVisible(false);
    }//GEN-LAST:event_SaveButtonActionPerformed

    private void HomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HomeActionPerformed
        // TODO add your handling code here:
        AK_MS_Lvl3.farrukhToBlame = false ;
        if ( Asim_users_login.accessLevel.equals ( "1" ) )
            {
                 AK_MS_Lvl1 main1 = new AK_MS_Lvl1 ( ) ;
                 main1.setVisible(true);
                 timerThread.stop();
                 super.dispose( ) ;
            }
            else if ( Asim_users_login.accessLevel.equals ( "2" ) )
            {
                 AK_MS_Lvl2 main2 = new AK_MS_Lvl2 ( ) ;
                 main2.setVisible(true);
                 timerThread.stop();
                 super.dispose( ) ;
            }

            else if ( Asim_users_login.accessLevel.equals ( "3" ) ) 
            {
                 AK_MS_Lvl3 main3 = new AK_MS_Lvl3 ( ) ;
                 main3.setVisible(true);
                 timerThread.stop();
                 super.dispose( ) ;
            } 
    }//GEN-LAST:event_HomeActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Farrukh_LookUpProduct.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Farrukh_LookUpProduct.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Farrukh_LookUpProduct.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Farrukh_LookUpProduct.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Farrukh_LookUpProduct().setVisible(true);
               // System.out.println("Running");
                 //Asim_TimerThreadClass TTC=new Asim_TimerThreadClass(Date_ ,Time_);
               
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ColNameLabel;
    private javax.swing.JLabel ColValue;
    private javax.swing.JLabel ColumnNameLabel;
    private javax.swing.JLabel CurrentValueLable;
    private javax.swing.JLabel DateLabel;
    private javax.swing.JButton DeleteButton;
    private javax.swing.JButton EditButton;
    private javax.swing.JButton Home;
    private javax.swing.JPanel LowerPanel;
    private javax.swing.JLabel MainScreenText;
    private javax.swing.JPanel MiddlePanel;
    private javax.swing.JLabel NewValueLabel;
    private javax.swing.JRadioButton PROD_BUTTON;
    private javax.swing.JRadioButton PROD_ID;
    private javax.swing.JRadioButton PROD_NAME;
    private javax.swing.JRadioButton PROD_PRINT;
    private javax.swing.JRadioButton PROD_SIZE;
    private javax.swing.JRadioButton PROD_STYLE;
    private javax.swing.JRadioButton PROD_THREAD;
    private javax.swing.JRadioButton PROD_TIME;
    private javax.swing.JRadioButton PROD_TYPE;
    private javax.swing.JRadioButton PROD_ZIP;
    private javax.swing.JTable Product_Table;
    private javax.swing.JButton SaveButton;
    private javax.swing.JButton SearchButton;
    private javax.swing.JTextField SearchTextField;
    private javax.swing.JLabel TimeLabel;
    private javax.swing.JPanel UpperPanel;
    private javax.swing.JComboBox<String> ValueOptions;
    private javax.swing.JPanel bgPanel;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
