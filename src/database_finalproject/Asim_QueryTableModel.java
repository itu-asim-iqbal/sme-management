/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database_finalproject;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static java.lang.System.in;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import java.lang.ArrayIndexOutOfBoundsException;

/**
 *
 * @author MohammadAsim
 */
public class Asim_QueryTableModel extends AbstractTableModel
{
    Vector cache;
    int colCount;
    String[] headers;
    Connection db;
    Statement statement;
    String currentURL;
    protected String Schema="scott";
    protected String USER;
    protected String PASS;
    protected String selectedRadio ;
    protected static final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    protected static final String DB_URL = "jdbc:oracle:thin:@localhost:1521:orcl";
    JCheckBox [ ] cbArray ;
    int fontSize = 20 ;
    protected String Exception="";
    
    public Asim_QueryTableModel()
    {
        cache = new Vector();
        //new gsl.sql.driv.Driver();
    }
    
    public String getColumnName( int i ) 
    {
        return headers [ i ] ;
    }

    @Override
    public int getRowCount() 
    {
        return cache.size();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getColumnCount() 
    {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return colCount;        
    }   

    @Override
    public Object getValueAt(int row, int col) throws ArrayIndexOutOfBoundsException
    {
        Exception=null;
        try
        {
            return ( ( String[ ] ) cache.elementAt ( row ) ) [ col ]  ;
        }
        catch(ArrayIndexOutOfBoundsException e)
                {
                    Exception=e.getMessage();
                    System.out.println("Exception: Index out of range!");
                    System.out.println("7777777777");
                    return null;
                }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void setHostURL ( String url )
    {
//        if ( url.equals ( currentURL ) )
//        {
//            return ;
//        }
        closeDB( ) ;        
        initDB ( url ) ;
        currentURL = url ;
    }
    
    public void setQuery ( String q )
    {
        Exception="";
        setHostURL ( DB_URL) ;
        cache = new Vector ( ) ;
        try
        {
            ResultSet rs = statement.executeQuery ( q ) ;
            ResultSetMetaData meta = rs.getMetaData ( ) ;
            colCount = meta.getColumnCount ( ) ;
            
            headers = new String [ colCount ] ;
            for ( int h = 1 ; h <= colCount ; h++ )
            {
                headers [ h - 1 ] = meta.getColumnName ( h ) ;
            }
            
            while ( rs.next ( ) )
            {
                String [ ] record = new String [ colCount ] ;
                for ( int i = 0 ; i < colCount ; i++ )
                {
                    record [ i ] = rs.getString ( i + 1 ) ;
                }
                cache.addElement ( record ) ;                
            }
            fireTableChanged ( null ) ;
        }
        catch ( Exception e )
        {
            Exception=e.getMessage();
            System.out.println("Exception: "+e.getMessage());
            System.out.println("8888888888");
            cache = new Vector ( ) ;
            e.printStackTrace( ) ;
            throw new RuntimeException("SQL Exception");
        }
    }
    
    public void insertQuery ( String q )
    {
        setHostURL ( DB_URL ) ;
        try
        {
            ResultSet rs = statement.executeQuery ( q ) ;
        }
        catch ( Exception e )
        {
            cache = new Vector ( ) ;
            e.printStackTrace( ) ;
        }
    }
    
    public void setCombo ( String q , String l1 , String l2 , String l3 , JComboBox com  )
    {
        setHostURL ( DB_URL ) ;
        try
        {
             ResultSet rs = statement.executeQuery ( q ) ;
             while ( rs.next( ) )
             {
                 String name = rs.getString ( l1 ) ;
                 if ( ! l2.equals( "" ) )
                 {
                     name += "-" + rs.getString ( l2 ) ;                 
                 }
                 
                 if ( ! l3.equals( "" ) )
                 {
                      name += "-" + rs.getString ( l3 ) ;
                 }
                 com.addItem ( name ) ;
             }
             fireTableChanged ( null ) ;
        }
        catch ( Exception e )
        {
            e.printStackTrace ( ) ;
        }
    }
    
    public void setCB ( JPanel cbPanel , JTable myTable )
    {
        cbArray = new JCheckBox [ colCount ] ;
        cbPanel.setLayout(new BoxLayout(cbPanel , BoxLayout.PAGE_AXIS));
        int i ;
        for ( i = 1 ; i <= colCount ; i++ )
        {
            String tempCBName = "CB_" + headers [ i - 1 ]  ;
            JCheckBox temp = new JCheckBox( headers [ i - 1 ] ) ;
            temp.setName ( tempCBName ) ;
            temp.setFont(new Font("Calibri", Font.BOLD, fontSize));
            cbArray [ i - 1 ] = temp ;
            temp.addActionListener ( new ActionListener ( ) 
            {
                @Override
                public void actionPerformed ( ActionEvent cb )
                {
//                    TableColumn col = myTable.getColumnModel( ).getColumn ( 0 ) ;
                    TableColumn col = myTable.getColumnModel( ).getColumn(myTable.getColumnModel( ).getColumnIndex(temp.getText()));
                    if ( temp.isSelected() )
                    {
                        col.setMaxWidth(2000); 
                        col.setMinWidth(125); 
                        col.setWidth(125); 
                        col.setPreferredWidth(125);
                    }
                    else
                    {
                        col.setMaxWidth(0); 
                        col.setMinWidth(0); 
                        col.setWidth(0); 
                        col.setPreferredWidth(0);
                    }
                }
            }
            );
            temp.setSelected ( true ) ;
            cbPanel.add ( temp ) ;
        }
    }
    
    public void checkAllCB ( JTable myTable )
    {
        for ( int i = 0 ; i < cbArray.length ; i++ )
        {
            TableColumn col = myTable.getColumnModel( ).getColumn( myTable.getColumnModel( ).getColumnIndex(cbArray [ i ].getText( ) ) ) ;                
            if ( cbArray [ i ].isSelected( ) )
            {
                col.setMaxWidth(2000); 
                col.setMinWidth(125); 
                col.setWidth(125); 
                col.setPreferredWidth(125);
            }
            else
            {
                col.setMaxWidth(0); 
                col.setMinWidth(0); 
                col.setWidth(0); 
                col.setPreferredWidth(0);
            }
        }
    }
    
    public void setRB ( JPanel rbPanel , ButtonGroup rbGroup )
    {
        rbPanel.setLayout(new GridLayout(3 , colCount/3));
        for ( int i = 1 ; i <= colCount ; i++ )
        {
            JRadioButton temp2 = new JRadioButton ( headers [ i - 1 ] ) ;
            if ( headers [ i - 1 ].contains("DATE" ) )
            {
                temp2.setText( temp2.getText ( ) + " (DD-MM-YYYY)" ) ;
                temp2.setFont(new Font("Calibri", Font.BOLD, fontSize-4));
            }
            else
            {
                temp2.setFont(new Font("Calibri", Font.BOLD, fontSize));
            }
            if ( i == 1 )
            {
                temp2.setSelected ( true ) ;
                selectedRadio = temp2.getText( ) ;                
            }
            temp2.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e) 
                {
                    selectedRadio = temp2.getText();
                }                    
            });
            rbGroup.add ( temp2 ) ;
            rbPanel.add ( temp2 )  ;
        }
    }
    
    public void initDB ( String url )
    {
        Exception="";
        try 
        {
            Class.forName ( JDBC_DRIVER ) ;
        }
        catch (final Exception e) 
        {
            Exception=e.getMessage();
            System.out.println("Exception: "+e.getMessage());
            System.out.println("2222222");
            throw new RuntimeException("Driver failure");
        }
        
        try
        {
            db = DriverManager.getConnection ( DB_URL , USER , PASS ) ;
        }
        catch (final Exception e)
        {
            Exception=e.getMessage();
            System.out.println("Exception: "+e.getMessage());
            System.out.println("3333333");
            throw new RuntimeException("Login failure");
        }
        
        try
        {
            statement = db.createStatement ( ) ;
        }
        catch (final Exception e) 
        {
            Exception=e.getMessage();
            System.out.println("Exception: "+e.getMessage());
            System.out.println("44444444");
            throw new RuntimeException("SQL failure");
        }
        try
        {
            alterSession();
        }
        catch (final Exception e) 
        {
            Exception=e.getMessage();
            System.out.println("Exception: "+e.getMessage());
            System.out.println("55555555");
            throw new RuntimeException("SQL failure");
        }        
    }
    
    public void closeDB ( ) 
    {
        Exception="";
        try 
        {
            if ( statement != null )
            {
                statement.close( ) ;
            }
            if ( db != null )
            {
                db.close( ) ;
                //USER = "" ;
                //PASS = "" ;
            }
        }
        catch (Exception e) 
        {
            Exception=e.getMessage();
            System.out.println("Could not close the current connection.");
            System.out.println("66666666");
            e.printStackTrace();
        }
    }
    
//    public void checkCB ( )
//    {
//        
//    }
    
    public void createNewDbUser(String userName, String Password)
    {
        String user="create user "+userName+" identified by "+Password;
        String grant="grant dba to "+userName;
        String setSch="ALTER SESSION SET CURRENT_SCHEMA="+Schema;
        //setQuery(user);
        //this.closeDB();
        //simpleQuery(grant);

        
        //tcloseDB();
        //simpleQuery(setSch);
       // this.closeDB();
    }
    
    public void alterSession()
    {
        Exception="";
        try
        {
            statement.execute( "alter session set current_schema="+Schema ) ;
            
        }
        catch ( Exception e )
        {
            Exception=e.getMessage();
            e.printStackTrace( ) ;
            throw new RuntimeException("SQL Exception");
        }
    }
    public String getVal ( String q , String l )
    {
        String val = null ;
        setHostURL ( DB_URL ) ;
        try
        {
             ResultSet rs = statement.executeQuery ( q ) ;
             if ( rs.next( ) )
             {
                     val = rs.getString ( l ) ;                 
             }             
        }
        catch ( Exception e )
        {
            e.printStackTrace ( ) ;
        }
        
        return val ;
    }
    
    
}
